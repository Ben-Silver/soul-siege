﻿using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NotPublic
{
    public class CardInitialiser : Singleton<CardInitialiser>
    {
        public List<Card> cards;
        private List<MockCard> mockCards;

        // Start is called before the first frame update
        void Start()
        {
            string path = Application.dataPath + "/JSON/Card.json";
            string jsonString = File.ReadAllText(path);
            //Debug.Log(jsonString);
            //cards = JsonHelper.FromJson<Card>(jsonString).ToList();
            mockCards = JsonHelper.FromJson<MockCard>(jsonString).ToList();

            for (int i = 0; i < mockCards.Count; i++)
            {
                Card newCard = ScriptableObject.CreateInstance<Card>();
                newCard.Initialise(mockCards[i]);
                cards.Add(newCard);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}