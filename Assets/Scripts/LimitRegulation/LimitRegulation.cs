﻿using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LimitRegulation : Singleton<LimitRegulation>
{
    public List<CardString> limitRegulation;

    private string path = "/JSON/LimitRegulation.json";

    void Start()
    {
        LoadLimitRegulation();
    }

    private void LoadLimitRegulation()
    {
        string jsonString = File.ReadAllText(Application.dataPath + path);
        limitRegulation = JsonHelper.FromJson<CardString>(jsonString).ToList();
    }
}

[System.Serializable] public class CardString
{
    public string cardName;
    public int numberAllowed;
}
