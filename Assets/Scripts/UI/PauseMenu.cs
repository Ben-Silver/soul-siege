﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    /* Public Variables */
    public GameObject pauseMenuWindow;
    public GameObject deckEditorSelectWindow;
    public DeckEditor deckEditorWindow;
    public bool menuOpen;
    public bool editingDeck;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            pauseMenuWindow.SetActive(true);
            menuOpen = true;
            pauseMenuWindow.GetComponent<Animator>().SetBool("menuOpen", true);
        }
    }

    /// <summary>
    /// Called whenever a button on the menu is pressed
    /// </summary>
    public void OnMenuButtonClick(string buttonName)
    {
        // Check the string associated with the button
        if (buttonName.Equals("Deck Edit"))
        {
            // Open the deck editor selection window
            deckEditorSelectWindow.SetActive(true);
        }
        else if (buttonName.Equals("Exit"))
        {
            // Close the menu
            pauseMenuWindow.GetComponent<Animator>().SetBool("menuOpen", false);
        }
    }

    /// <summary>
    /// Function to open the deck editor
    /// </summary>
    public void OpenDeckEditor(int deckIndex)
    {
        if (deckIndex.Equals(-1))
        {
            // Close the deck selection screen
            deckEditorSelectWindow.SetActive(false);
        }
        else
        {
            // Open the deck editor window
            deckEditorWindow.gameObject.SetActive(true);

            // Load the deck list
            deckEditorWindow.LoadDeckList(deckIndex);

            // Register the player as editing the deck
            editingDeck = true;
        }
    }

    /// <summary>
    /// Function to exit the deck editor
    /// </summary>
    public void ExitDeckEditor()
    {
        bool hasBannedCards = deckEditorWindow.CheckForBannedCards();

        if (!hasBannedCards)
        {
            deckEditorSelectWindow.SetActive(true);
            deckEditorWindow.ClearTempCollection();
            deckEditorWindow.DeselectCard();
            deckEditorWindow.gameObject.SetActive(false);
            editingDeck = false;
        }
        else
        {
            Debug.Log("Chet");
        }
    }

    /// <summary>
    /// Sets the internal visibility of the PauseMenuWindow
    /// </summary>
    /// <param name="isMenuOpen">The bool being bassed in</param>
    public void SetMenuOpen(bool isMenuOpen)
    {
        // Assign the passed value to menuOpen
        menuOpen = isMenuOpen;
    }
}
