﻿using NotPublic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DeckEditor : MonoBehaviour
{
    /* Public Variables */
    [Header("UI Components")]
    public Image cardCollectionWindow;
    public Image deckWindow;
    public GameObject cardInfoTab;
    public Button addCardButton;
    public Button removeCardButton;
    public TextMeshProUGUI deckName;

    [Header("Info Components")]
    public TextMeshProUGUI cardName;
    public TextMeshProUGUI cardLore;
    public TextMeshProUGUI cardLevel;
    public TextMeshProUGUI cardStats;

    [Header("Card Components")]
    public DeckList currentDeckList;
    public List<DeckList> deckList;
    //public List<Card> cardCollection;
    public DeckList cardCollection;
    public int loadedDeckIndex;
    public InterfaceCard interfaceCard;

    /* Private Variables */
    private InterfaceCard currentSelectedCard;
    private List<InterfaceCard> tempDeckList = new List<InterfaceCard>();
    private List<InterfaceCard> tempCardCollection = new List<InterfaceCard>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    /// <summary>
    /// Function to save the current deck list
    /// </summary>
    private void SaveDeckList(int deckIndex)
    {
        deckList[loadedDeckIndex] = currentDeckList;
    }
    
    /// <summary>
    /// Function to load a deck list
    /// </summary>
    public void LoadDeckList(int deckIndex)
    {
        currentDeckList = deckList[deckIndex];
        deckName.text = currentDeckList.deckName;
        LoadCardCollection();

        for (int i = 0; i < currentDeckList.cards.Count; i++)
        {
            GameObject newCard = Instantiate(interfaceCard.gameObject);
            newCard.GetComponent<InterfaceCard>().card = currentDeckList.cards[i];
            newCard.GetComponent<InterfaceCard>().cardQuantity = 1;
            newCard.GetComponent<Image>().sprite = currentDeckList.cards[i].cardSprite;
            newCard.GetComponent<InterfaceCard>().SetEditorTabParent(deckWindow.transform);
            tempDeckList.Add(newCard.GetComponent<InterfaceCard>());
        }
    }

    /// <summary>
    /// Function to load the player's card collection
    /// </summary>
    public void LoadCardCollection()
    {
        for (int i = 0; i < cardCollection.cards.Count; i++)
        {
            GameObject newCard = Instantiate(interfaceCard.gameObject);
            newCard.GetComponent<InterfaceCard>().card = cardCollection.cards[i];
            newCard.GetComponent<InterfaceCard>().cardQuantity = cardCollection.cardQuantities[i];
            newCard.GetComponent<Image>().sprite = cardCollection.cards[i].cardSprite;
            newCard.GetComponent<InterfaceCard>().SetEditorTabParent(cardCollectionWindow.transform);
            tempCardCollection.Add(newCard.GetComponent<InterfaceCard>());
        }
    }

    /// <summary>
    /// Clears the temporary inventory
    /// </summary>
    public void ClearTempCollection()
    {
        while (tempCardCollection.Count > 0)
        {
            Destroy(tempCardCollection[0].gameObject);
            tempCardCollection.RemoveAt(0);
        }

        while (tempDeckList.Count > 0)
        {
            Destroy(tempDeckList[0].gameObject);
            tempDeckList.RemoveAt(0);
        }
    }

    /// <summary>
    /// Function to update the collection if a card is added to the deck
    /// </summary>
    public void UpdateCollection()
    {
        // 
    }

    /// <summary>
    /// When a card in the inventory is selected
    /// </summary>
    public void OnButtonClicked()
    {
        cardInfoTab.SetActive(true);
    }

    /// <summary>
    /// Function to select a card from the inventory
    /// </summary>
    /// <param name="selectedCard">The card that has been selected</param>
    public void SelectCard(InterfaceCard selectedCard)
    {
        currentSelectedCard = null;
        currentSelectedCard = selectedCard;

        if (currentSelectedCard.GetEditorTabParent().gameObject.name.Equals("CardCollectionWindow"))
        {
            addCardButton.interactable = true;
            removeCardButton.interactable = false;
        }
        else if (currentSelectedCard.GetEditorTabParent().gameObject.name.Equals("DeckWindow"))
        {
            addCardButton.interactable = false;
            removeCardButton.interactable = true;
        }
        
        cardName.text = selectedCard.card.cardName;
        cardLore.text = selectedCard.card.cardEffect;

        if (selectedCard.card.cardType.Equals(CardType.Monster))
        {
            cardLevel.text = "Level " + selectedCard.card.cardSoulEnergy.ToString();
            cardStats.text = selectedCard.card.cardATK.ToString() + " / " + selectedCard.card.cardDEF.ToString();
        }
        else
        {
            cardLevel.text = "";
            cardStats.text = "";
        }
    }

    /// <summary>
    /// Function to update the deck list
    /// </summary>
    private void UpdateDeckList()
    {
        ClearTempCollection();
        LoadDeckList(loadedDeckIndex);
    }

    /// <summary>
    /// Function to add a card to the deck 
    /// </summary>
    public void AddCardToDeck()
    {
        int quantityChecker = 0;

        foreach (Card c in deckList[loadedDeckIndex].cards)
        {
            if (c.cardName.Equals(currentSelectedCard.card.cardName))
            {
                if (quantityChecker < 3)
                {
                    quantityChecker += 1;
                }
                else
                {
                    return;
                }
            }
        }

        if (quantityChecker < 3)
        {
            if (currentSelectedCard.cardQuantity >= 1)
            {
                for (int i = 0; i < cardCollection.cards.Count; i++)
                {
                    if (currentSelectedCard.card.Equals(cardCollection.cards[i]))
                    {
                        cardCollection.cardQuantities[i] -= 1;
                        //currentSelectedCard.cardQuantity -= 1;
                    }
                }

                deckList[loadedDeckIndex].cards.Add(currentSelectedCard.card);
            }
            else
            {
                return;
            }

            UpdateDeckList();
        }
    }

    /// <summary>
    /// Function to remove a card from the deck 
    /// </summary>
    public void RemoveCardFromDeck()
    {
        for (int i = 0; i < deckList[loadedDeckIndex].cards.Count; i++)
        {
            if (deckList[loadedDeckIndex].cards[i].Equals(currentSelectedCard.card))
            {
                for (int j = 0; j < cardCollection.cards.Count; j++)
                {
                    if (cardCollection.cards[j].Equals(currentSelectedCard.card))
                    {
                        cardCollection.cardQuantities[j] += 1;
                    }
                    /*else if (j.Equals(cardCollection.cards.Count - 1) && cardCollection.cards[j] != currentSelectedCard.card)
                    {
                        // Add the card to the collection
                        cardCollection.cards.Add(currentSelectedCard.card);

                        // Set the card quantity to 1
                        cardCollection.cardQuantities.Add(1);
                    }*/
                }

                deckList[loadedDeckIndex].cards.Remove(currentSelectedCard.card);
                UpdateDeckList();

                return;
            }
        }
    }

    public List<InterfaceCard> GetTempDecklist()
    {
        return tempDeckList;
    }

    /// <summary>
    /// Clears the selected card
    /// </summary>
    public void DeselectCard()
    {
        currentSelectedCard = null;
    }

    public bool CheckForBannedCards()
    {
        bool hasBannedCards = false;
        List<string> names = new List<string>();
        List<int> qtys = new List<int>();

        for (int i = 0; i < tempDeckList.Count; i++)
        {
            if (names.Contains(tempDeckList[i].card.cardName))
            {
                for (int j = 0; j < names.Count; j++)
                {
                    if (names[j] == tempDeckList[i].card.cardName)
                    {
                        qtys[j] += 1;
                    }
                }
            }
            else
            {
                names.Add(tempDeckList[i].card.cardName);
                qtys.Add(1);
            }

            for (int j = 0; j < names.Count; j++)
            {
                for (int k = 0; k < LimitRegulation.Instance.limitRegulation.Count; k++)
                {
                    if (names[j] == LimitRegulation.Instance.limitRegulation[k].cardName && qtys[j] > LimitRegulation.Instance.limitRegulation[k].numberAllowed)
                    {
                        hasBannedCards = true;
                        break;
                    }
                }
            }
        }

        return hasBannedCards;
    }
}
