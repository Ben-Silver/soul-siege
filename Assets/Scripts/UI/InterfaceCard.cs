﻿using NotPublic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InterfaceCard : MonoBehaviour
{
    [Header("Configuration")]
    public Card card;

    [Header("Quantity")]
    public TextMeshProUGUI cardQuantityText;
    public int cardQuantity;
    
    [Header("Rarity")]
    public Image rarityIcon;
    public Sprite[] rarityIcons;
    
    [Header("Limitation")]
    public Image limitIcon;
    public Sprite[] limitIcons;

    private DeckEditor deckEditor;
    private Button button;
    private Transform editorTabParent;
    private bool cardSelected = false;
    private bool onList = false;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        
        if (card.cardRarity == Rarity.Normal)
        {
            rarityIcon.sprite = rarityIcons[0];
        }
        else if (card.cardRarity == Rarity.Rare)
        {
            rarityIcon.sprite = rarityIcons[1];
        }
        else if (card.cardRarity == Rarity.SuperRare)
        {
            rarityIcon.sprite = rarityIcons[2];
        }
        else if (card.cardRarity == Rarity.UltraRare)
        {
            rarityIcon.sprite = rarityIcons[3];
        }

        for (int i = 0; i < LimitRegulation.Instance.limitRegulation.Count; i++)
        {
            if (LimitRegulation.Instance.limitRegulation[i].cardName == card.cardName)
            {
                limitIcon.sprite = limitIcons[LimitRegulation.Instance.limitRegulation[i].numberAllowed];
                onList = true;
            }
        }

        if (!onList)
        {
            limitIcon.sprite = null;
            limitIcon.color = new Color(1, 1, 1, 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        cardQuantityText.text = cardQuantity.ToString();

        if (cardQuantity == 0)
        {
            //button.interactable = false;
            button.image.color = new Color(button.image.color.r, button.image.color.g, button.image.color.g, 0.5f);
        }
        else
        {
            //button.interactable = true;
            button.image.color = new Color(button.image.color.r, button.image.color.g, button.image.color.g, 1.0f);
        }

        if (!deckEditor)
        {
            deckEditor = FindObjectOfType<DeckEditor>();
        }
    }

    /// <summary>
    /// Called when a card is clicked
    /// </summary>
    public void OnCardClicked()
    {
        if (FindObjectOfType<DeckEditor>().isActiveAndEnabled)
        {
            deckEditor.SelectCard(this);
        }
        else if (FindObjectOfType<Shop>().isActiveAndEnabled)
        {
            FindObjectOfType<Shop>().SelectCard(this);
        }
    }

    /// <summary>
    /// Function to set the parent of the interface card
    /// </summary>
    /// <param name="parent">The transform this object is being parented to</param>
    public void SetEditorTabParent(Transform parent)
    {
        editorTabParent = parent;
        transform.SetParent(editorTabParent);

        if (editorTabParent.gameObject.name.Equals("DeckWindow"))
        {
            cardQuantityText.gameObject.SetActive(false);
        }
        else
        {
            cardQuantityText.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// Function to get the parent of the interface card
    /// </summary>
    public Transform GetEditorTabParent()
    {
        return editorTabParent;
    }

    public int GetLimit(string name)
    {
        //bool isAllowed = true;
        int limit = 3;

        for (int i = 0; i < LimitRegulation.Instance.limitRegulation.Count; i++)
        {
            if (LimitRegulation.Instance.limitRegulation[i].cardName == card.cardName)
            {
                limit = LimitRegulation.Instance.limitRegulation[i].numberAllowed;
                break;
            }
        }

        return limit;
    }

    public int GetLimit()
    {
        //bool isAllowed = true;
        int limit = 3;

        for (int i = 0; i < LimitRegulation.Instance.limitRegulation.Count; i++)
        {
            if (LimitRegulation.Instance.limitRegulation[i].cardName == card.cardName)
            {
                limit = LimitRegulation.Instance.limitRegulation[i].numberAllowed;
                break;
            }
        }

        return limit;
    }
}
