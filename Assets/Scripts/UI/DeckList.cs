﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NotPublic
{
    [CreateAssetMenu]
    public class DeckList : ScriptableObject
    {
        /* Public Variables */
        public string deckName;
        public List<Card> cards;
        public List<int> cardQuantities;
    }
}