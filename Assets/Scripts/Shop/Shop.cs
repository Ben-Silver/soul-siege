﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace NotPublic
{
    public class Shop : MonoBehaviour
    {
        /* Public Variables */
        [Header("Pack List Window Components")]
        public GameObject packListWindow;
        public TextMeshProUGUI packInfoTitle;
        public TextMeshProUGUI packInfoDescription;

        [Header("UI Components")]
        public TextMeshProUGUI money;
        public TextMeshProUGUI cost;
        public InterfaceCard interfaceCard;

        [Header("Pack List Components")]
        public int selectedPackIndex = 0;
        public List<Pack> packList;

        /* Private Variables */
        private List<InterfaceCard> tempPackList;
        private int packCost = 50;

        // Start is called before the first frame update
        void Start()
        {
            // Initialise tempPackList
            tempPackList = new List<InterfaceCard>();

            // Load the pack list at 0
            LoadPackList(selectedPackIndex);
        }

        private void OnDisable()
        {
            packList[selectedPackIndex].packLoaded = false;
        }

        // Update is called once per frame
        void Update()
        {
            money.text = "Money: " + 50000.ToString();
            cost.text = "Packs Cost: " + packCost.ToString();
        }

        /// <summary>
        /// Called when a pack is selected
        /// </summary>
        public void OnPackSelected(int packIndex)
        {
            // If the pack is not yet loaded
            if (!packList[packIndex].packLoaded)
            {
                // Unload the previous back
                UnloadPackList(selectedPackIndex);
                packList[selectedPackIndex].packLoaded = false;

                // Set selectedPackIndex to the value passed in
                selectedPackIndex = packIndex;

                // Load the list of the selected pack
                LoadPackList(selectedPackIndex);

                Debug.Log("Loading pack");
            }
            else
            {
                Debug.Log("Pack is already loaded");
            }
        }

        /// <summary>
        /// Loads the pack list at the passed index
        /// </summary>
        private void LoadPackList(int packIndex)
        {
            // Get the title and description of the selected pack
            packInfoTitle.text = packList[packIndex].packName;
            packInfoDescription.text = packList[packIndex].packDescription;

            #region OLD CODE
            // Loop through the ultra rare cards
            /*for (int i = 0; i < packList[packIndex].packList.ultraRareCards.Length; i++)
            {
                // Instantiate a the card at i as a new gameObject
                GameObject newCard = Instantiate(interfaceCard.gameObject);

                // Set the card in the new interface card
                newCard.GetComponent<InterfaceCard>().card = packList[packIndex].packList.ultraRareCards[i];

                // Set the card quantity of the new interface card
                newCard.GetComponent<InterfaceCard>().cardQuantity = 1;

                // Set the card's sprite in the new interface card
                newCard.GetComponent<Image>().sprite = packList[packIndex].packList.ultraRareCards[i].cardSprite;

                // Parent the new gameObject to the card collection window
                newCard.GetComponent<InterfaceCard>().SetEditorTabParent(packListWindow.transform);

                // Add the card to the temp list
                tempPackList.Add(newCard.GetComponent<InterfaceCard>());
            }

            // Loop through the super rare cards
            for (int i = 0; i < packList[packIndex].packList.superRareCards.Length; i++)
            {
                // Instantiate a the card at i as a new gameObject
                GameObject newCard = Instantiate(interfaceCard.gameObject);

                // Set the card in the new interface card
                newCard.GetComponent<InterfaceCard>().card = packList[packIndex].packList.superRareCards[i];

                // Set the card quantity of the new interface card
                newCard.GetComponent<InterfaceCard>().cardQuantity = 2;

                // Set the card's sprite in the new interface card
                newCard.GetComponent<Image>().sprite = packList[packIndex].packList.superRareCards[i].cardSprite;

                // Parent the new gameObject to the card collection window
                newCard.GetComponent<InterfaceCard>().SetEditorTabParent(packListWindow.transform);

                // Add the card to the temp list
                tempPackList.Add(newCard.GetComponent<InterfaceCard>());
            }

            // Loop through the rare cards
            for (int i = 0; i < packList[packIndex].packList.rareCards.Length; i++)
            {
                // Instantiate a the card at i as a new gameObject
                GameObject newCard = Instantiate(interfaceCard.gameObject);

                // Set the card in the new interface card
                newCard.GetComponent<InterfaceCard>().card = packList[packIndex].packList.rareCards[i];

                // Set the card quantity of the new interface card
                newCard.GetComponent<InterfaceCard>().cardQuantity = 4;

                // Set the card's sprite in the new interface card
                newCard.GetComponent<Image>().sprite = packList[packIndex].packList.rareCards[i].cardSprite;

                // Parent the new gameObject to the card collection window
                newCard.GetComponent<InterfaceCard>().SetEditorTabParent(packListWindow.transform);

                // Add the card to the temp list
                tempPackList.Add(newCard.GetComponent<InterfaceCard>());
            }

            // Loop through the normal cards
            for (int i = 0; i < packList[packIndex].packList.normalCards.Length; i++)
            {
                // Instantiate a the card at i as a new gameObject
                GameObject newCard = Instantiate(interfaceCard.gameObject);

                // Set the card in the new interface card
                newCard.GetComponent<InterfaceCard>().card = packList[packIndex].packList.normalCards[i];

                // Set the card quantity of the new interface card
                newCard.GetComponent<InterfaceCard>().cardQuantity = 8;

                // Set the card's sprite in the new interface card
                newCard.GetComponent<Image>().sprite = packList[packIndex].packList.normalCards[i].cardSprite;

                // Parent the new gameObject to the card collection window
                newCard.GetComponent<InterfaceCard>().SetEditorTabParent(packListWindow.transform);

                // Add the card to the temp list
                tempPackList.Add(newCard.GetComponent<InterfaceCard>());
            }*/
            #endregion

            AppendPackListRarity(packList[packIndex].packList.ultraRareCards, "ultra");
            AppendPackListRarity(packList[packIndex].packList.superRareCards, "super");
            AppendPackListRarity(packList[packIndex].packList.rareCards, "rare");
            AppendPackListRarity(packList[packIndex].packList.normalCards, "normal");

            // Loop through the temp list
            for (int i = 0; i < tempPackList.Count; i++)
            {
                // Rename the object at i according to the value of i, its index value
                tempPackList[i].gameObject.name = i.ToString();
            }

            // Set the pack list as loaded
            packList[selectedPackIndex].packLoaded = true;
        }

        /// <summary>
        /// Unloads the previos pack list
        /// </summary>
        private void UnloadPackList(int packIndex)
        {
            // While the list isn't empty
            while (tempPackList.Count > 0)
            {
                // Destroy the first card in the list in the scene
                Destroy(tempPackList[0].gameObject);

                // Remove the corresponding card from the list
                tempPackList.RemoveAt(0);
            }
        }

        /// <summary>
        /// Appends a passed array of rarities to a temp list
        /// </summary>
        private void AppendPackListRarity(Card[] rarityList, string rarity)
        {
            // Loop through the passed rarity array
            for (int i = 0; i < rarityList.Length; i++)
            {
                // Instantiate a the card at i as a new gameObject
                GameObject newCard = Instantiate(interfaceCard.gameObject);

                // Set the card in the new interface card
                newCard.GetComponent<InterfaceCard>().card = rarityList[i];

                if (rarity.Equals("ultra".ToLower()))
                {
                    // Set the card quantity of the new interface card
                    newCard.GetComponent<InterfaceCard>().cardQuantity = packList[selectedPackIndex].packList.cardQuantities[i];
                }
                else if (rarity.Equals("super".ToLower()))
                {
                    // Set the card quantity of the new interface card
                    newCard.GetComponent<InterfaceCard>().cardQuantity = packList[selectedPackIndex].packList.cardQuantities[i + 4];
                }
                else if (rarity.Equals("rare".ToLower()))
                {
                    // Set the card quantity of the new interface card
                    newCard.GetComponent<InterfaceCard>().cardQuantity = packList[selectedPackIndex].packList.cardQuantities[i + 4 + 6];
                }
                else if (rarity.Equals("normal".ToLower()))
                {
                    // Set the card quantity of the new interface card
                    newCard.GetComponent<InterfaceCard>().cardQuantity = packList[selectedPackIndex].packList.cardQuantities[i + 4 + 6 + 12];
                }

                // Set the card's sprite in the new interface card
                newCard.GetComponent<Image>().sprite = rarityList[i].cardSprite;

                // Parent the new gameObject to the card collection window
                newCard.GetComponent<InterfaceCard>().SetEditorTabParent(packListWindow.transform);

                // Add the card to the temp list
                tempPackList.Add(newCard.GetComponent<InterfaceCard>());
            }
        }

        public void SelectCard(InterfaceCard card)
        {

        }
    }
}