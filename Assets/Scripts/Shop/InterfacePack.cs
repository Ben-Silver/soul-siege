﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace NotPublic
{
    public class InterfacePack : MonoBehaviour
    {
        /* Public Variables */
        public Pack pack;
        public Image packSprite;
        public PackList packList;
        public TextMeshProUGUI packDescription;

        // Start is called before the first frame update
        void Start()
        {
            // Get the pack sprite
            packSprite.sprite = pack.packSprite;

            // Get the pack list
            packList = pack.packList;

            // Get the pack description
            packDescription.text = pack.packDescription;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}