﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NotPublic
{
    [CreateAssetMenu]
    public class Pack : ScriptableObject
    {
        /* Public Variables */
        public string packName;
        [TextArea(1, 4)] public string packDescription;
        public Sprite packSprite;
        public PackList packList;
        public bool packLoaded;
        //public List<Card> cardList;

        private void LoadPackData()
        {
            Pack newPack = JsonUtility.FromJson<Pack>("Resources/Packs/" + packName + ".json");
        }
    }

    [System.Serializable]
    public class PackList
    {
        public Card[] ultraRareCards = new Card[4];
        public Card[] superRareCards = new Card[6];
        public Card[] rareCards = new Card[12];
        public Card[] normalCards = new Card[24];
        public int[] cardQuantities =
        {
        1, 1, 1, 1,                                                                 // 4 ultra rare slots, containing 1 ultra rare each
        2, 2, 2, 2, 2, 2,                                                           // 6 super rare slots, containing 2 super rares each
        4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,                                         // 12 rare slots, containing 4 rares each
        8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8      // 24 normal slots, containing 8 normals each
    };
    }
}