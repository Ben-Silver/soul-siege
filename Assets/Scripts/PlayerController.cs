﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    /* Public Variables */
    public MoveDirection lastMovedDirection;
    public Rigidbody rigidBody;
    public Animator animator;
    public int moveSpeed = 6;
    public int runSpeed = 10;
    public bool playerMoving;
    public bool playerRunning;

    /* Private Variables */
    private Vector3 moveInput;
    private Vector3 lastMoveDir;
    private Vector3 vertMovement;
    private Vector3 horiMovement;
    private Vector3 vel = Vector2.zero;
    private int currentMoveSpeed;

    // Start is called before the first frame update
    void Start()
    {
        // If there is no rigidbody assigned
        if (!rigidBody)
        {
            // Auto-find the rigidbody component
            rigidBody = GetComponent<Rigidbody>();
        }

        // If there is no animator assigned
        if (!animator)
        {
            // Auto-find the animator component
            animator = GetComponent<Animator>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Get the key inout
        moveInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        
        // If input is received, register the player as moving
        playerMoving = (moveInput != Vector3.zero) ? true : false;

        // If the run button is down, reguister the player as running
        playerRunning = (Input.GetKey(KeyCode.LeftShift) && playerMoving) ? true : false;

        // If the player is running, modify the currentMoveSpeed
        currentMoveSpeed = (playerRunning) ? runSpeed : moveSpeed;

        // If the player is moving
        if (playerMoving)
        {
            // Set the last moved direction of the player
            lastMoveDir = moveInput;

            // Set the direction of the player in lamens terms
            SetDirection();
        }

        // Set the value of playerMoving in the animator component
        animator.SetBool("playerMoving", playerMoving);

        // Set the value of playerRunning in the animator component
        animator.SetBool("playerRunning", playerRunning);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // If the player is moving
        if (playerMoving)
        {
            // Translate the player
            rigidBody.transform.Translate(Vector3.forward * currentMoveSpeed * Time.deltaTime);
        }
    }

    /// <summary>
    /// Set's the readable direction of the player
    /// </summary>
    private void SetDirection()
    {
        if (lastMoveDir.x == 0 && lastMoveDir.z == 1)
        {
            lastMovedDirection = MoveDirection.Forward;
            rigidBody.transform.localEulerAngles = new Vector3(0, 0, 0);
        }
        else if (lastMoveDir.x == 0 && lastMoveDir.z == -1)
        {
            lastMovedDirection = MoveDirection.Backward;
            rigidBody.transform.localEulerAngles = new Vector3(0, 180, 0);
        }
        else if (lastMoveDir.x == 1 && lastMoveDir.z == 0)
        {
            lastMovedDirection = MoveDirection.Right;
            rigidBody.transform.localEulerAngles = new Vector3(0, 90, 0);
        }
        else if (lastMoveDir.x == -1 && lastMoveDir.z == 0)
        {
            lastMovedDirection = MoveDirection.Left;
            rigidBody.transform.localEulerAngles = new Vector3(0, -90, 0);
        }
        else if (lastMoveDir.x == 1 && lastMoveDir.z == 1)
        {
            lastMovedDirection = MoveDirection.ForwardRight;
            rigidBody.transform.localEulerAngles = new Vector3(0, 45, 0);
        }
        else if (lastMoveDir.x == -1 && lastMoveDir.z == 1)
        {
            lastMovedDirection = MoveDirection.ForwardLeft;
            rigidBody.transform.localEulerAngles = new Vector3(0, -45, 0);
        }
        else if (lastMoveDir.x == 1 && lastMoveDir.z == -1)
        {
            lastMovedDirection = MoveDirection.BackwardRight;
            rigidBody.transform.localEulerAngles = new Vector3(0, 135, 0);
        }
        else if (lastMoveDir.x == -1 && lastMoveDir.z == -1)
        {
            lastMovedDirection = MoveDirection.BackwardLeft;
            rigidBody.transform.localEulerAngles = new Vector3(0, -135, 0);
        }
    }
}

/// <summary>
/// The direction the player is facing
/// </summary>
public enum MoveDirection
{
    None,
    Forward,
    Backward,
    Right,
    Left,
    ForwardRight,
    ForwardLeft,
    BackwardRight,
    BackwardLeft
}
