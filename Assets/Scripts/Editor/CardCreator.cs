﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CardCreator : EditorWindow
{
    private Texture2D headerSectionTexture;
    private Texture2D monsterTemplateTexture;
    private Texture2D spellTemplateTexture;
    private Texture2D equipmentTemplateTexture;

    private Color headerSectionColour = new Color(13.0f / 255.0f, 32.0f / 255.0f, 44.0f / 255.0f, 1.0f);
    private Rect headerSection;
    private Rect cardSection;

    [MenuItem("Window/Card Designer")]
    static void OpenWindow()
    {
        CardCreator window = (CardCreator)GetWindow(typeof(CardCreator));
        window.minSize = new Vector2(640, 480);
        window.Show();
    }

    /// <summary>
    /// Similar to Start() or Awake()
    /// </summary>
    void OnEnable()
    {
        // 
    }

    /// <summary>
    /// Initialise Texture2D values
    /// </summary>
    void InitTexture()
    {
        // 
    }

    /// <summary>
    /// Similar to Update()
    /// Not called once per frame, called one or more times per interaction
    /// </summary>
    void OnGUI()
    {
        // 
    }

    /// <summary>
    /// Defines Rect values and paints textures based on Rects
    /// </summary>
    void DrawLayouts()
    {
        // 
    }

    /// <summary>
    /// Draw contents of header
    /// </summary>
    void DrawHeader()
    {
        // 
    }

    /// <summary>
    /// Draw contents of monster
    /// </summary>
    void DrawMonsterSettings()
    {
        // 
    }

    /// <summary>
    /// Draw contents of spell
    /// </summary>
    void DrawSpellSettings()
    {
        // 
    }

    /// <summary>
    /// Draw contents of equipment
    /// </summary>
    void DrawEquipmentSettings()
    {
        // 
    }
}
