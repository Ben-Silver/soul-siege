﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    /* Public Variables */
    public Transform target;
    public float xOffset;
    public float yOffset;
    public float zOffset;

    /* Private Variables */
    private Vector3 vel = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        // If there is no target assigned
        if (!target)
        {
            // Auto-find the rigidbody component
            target = FindObjectOfType<PlayerController>().transform;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Smooth the camera to the player's position, taking the offsets into account
        transform.position = Vector3.SmoothDamp
        (
            transform.position,
            new Vector3
            (
                target.position.x + xOffset, 
                target.position.y + yOffset, 
                target.position.z + zOffset
            ),
            ref vel,
            0.1f
        );
    }
}
