﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Mathf.Abs(Input.GetAxisRaw("HorizontalJL")) > 0)
        {
            transform.Rotate(Vector3.up * Input.GetAxisRaw("HorizontalJL") * 50 * Time.deltaTime);
        }
    }
}
