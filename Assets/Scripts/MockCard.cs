﻿using NotPublic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]public class MockCard
{
    [Header("Card Details")]
    public string cardID;
    public string cardName;
    public CardType cardType;
    public MonsterType monsterType;
    public int cardSoulEnergy;
    public int cardATK;
    public int cardDEF;
    public string cardEffect;
    public Sprite cardSprite;

    [Header("Card Details")]
    public Rarity cardRarity;
    public int cardQuantity;
    public bool battledThisTurn;

    [Header("Card Effects")]
    public EffectType effectType;
    public int numberToDraw;
    public int numberToSummon;
    public SearchType searchType;
    public List<string> searchParams;
}
