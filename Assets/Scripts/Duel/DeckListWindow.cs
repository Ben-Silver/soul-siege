﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeckListWindow : MonoBehaviour
{
    /* Public Variables */
    public GameObject contentViewport;
    public GameObject cardInDeckPrefab;
    public CardActionWindow cardActionWindowDeck;

    public List<GameObject> parentedCards;

	// Use this for initialization
	void OnEnable()
    {
        for (int i = 0; i < GameManager.Instance.mainDeck.deck.Count; i++)
        {
            GameObject newCard = Instantiate(cardInDeckPrefab);

            newCard.gameObject.name = i.ToString();

            newCard.transform.SetParent(contentViewport.transform);

            parentedCards.Add(newCard);
        }
	}

    void OnDisable()
    {
        for (int i = parentedCards.Count - 1; i > -1; i--)
        {
            Destroy(parentedCards[i]);
        }

        parentedCards.Clear();
    }
	
	// Update is called once per frame
	void Update()
    {
		
	}
}