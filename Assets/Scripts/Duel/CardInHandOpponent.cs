﻿using NotPublic;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardInHandOpponent : MonoBehaviour
{
	public Image image;
	public Sprite blankDefault;
	public Card card;
	public int indexNumber;

	private int indexOutput = 0;

	// Use this for initialization
	void Start()
	{
		image = GetComponent<Image>();
		indexNumber = (int.TryParse(gameObject.name, out indexOutput)) ? indexOutput : -1;
	}
	
	// Update is called once per frame
	void Update()
	{
		if (indexNumber > -1 && indexNumber < GameManager.Instance.handOpp.hand.Count)
		{
			card = GameManager.Instance.handOpp.hand[indexNumber];

			if (card.cardSprite != null)
			{
				image.sprite = card.cardSprite;
			}
			else
			{
				image.sprite = blankDefault;
			}
		}
	}

    /// <summary>
    /// Function to play a card from the hand
    /// </summary>
	public void PlayCard()
	{
		if (card.cardType == CardType.Monster && GameManager.Instance.numberOfSummons > 0 && GameManager.Instance.soulEnergy > card.cardSoulEnergy)
		{
			for (int i = 0; i < GameManager.Instance.gameUI.cardZonesOpp.Length; i++)
			{
				if (GameManager.Instance.gameUI.cardZonesOpp[i].zoneType == ZoneType.Monster)
				{
					if (!GameManager.Instance.gameUI.cardZonesOpp[i].occupied)
					{
						GameManager.Instance.gameUI.cardZonesOpp[i].cardInZone = card;
						GameManager.Instance.handOpp.hand.RemoveAt(indexNumber);
						GameManager.Instance.numberOfSummons -= 1;
						GameManager.Instance.soulEnergy -= card.cardSoulEnergy;

						return;
					}
				}
			}
		}
		else if (card.cardType == CardType.Spell)
		{
            // Loop through the zones
			for (int i = 0; i < GameManager.Instance.gameUI.cardZonesOpp.Length; i++)
			{
                // If the zone at i is a spell zone
				if (GameManager.Instance.gameUI.cardZonesOpp[i].zoneType == ZoneType.Spell)
				{
                    // If the zone is empty
					if (!GameManager.Instance.gameUI.cardZonesOpp[i].occupied)
					{
                        // Remove the card from the hand and put it in the zone
						GameManager.Instance.gameUI.cardZonesOpp[i].cardInZone = card;
						GameManager.Instance.handOpp.hand.RemoveAt(indexNumber);

                        // Exit from the function
						return;
					}
				}
			}
		}
	}

	public void Discard()
	{
		GameManager.Instance.graveOpp.grave.Add(GameManager.Instance.handOpp.hand[indexNumber]);
		GameManager.Instance.handOpp.hand.RemoveAt(indexNumber);
	}
}
