﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NotPublic
{
	public class Hand : MonoBehaviour
	{
		/* Public Variables */
		public List<Card> hand;
		public List<Image> handSlots;

		// Use this for initialization
		void Start()
		{

		}

		// Update is called once per frame
		void Update()
		{
			for (int i = 0; i < handSlots.Count; i++)
			{
				if (i < hand.Count)
				{
					handSlots[i].gameObject.SetActive(true);
					//handSlots[i].GetComponent<Image>().color = hand[i].cardColour;
				}
				else
				{
					handSlots[i].gameObject.SetActive(false);
				}
			}
		}
	}
}