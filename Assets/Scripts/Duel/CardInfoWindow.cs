﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NotPublic
{
    public class CardInfoWindow : MonoBehaviour
    {
        /* Public Variables */
        public Card selectedCard;
        public Text cardNameText;
        public Text cardEffectText;
        public Text cardSoulEnergyText;
        public Text cardATKText;
        public Text cardDEFText;
        public Button backButton;
        public int selectedCardIndex;

        /* Private Variables */
        private string cardName;
        private string cardEffect;
        private string cardSoulEnergy;
        private string cardATK;
        private string cardDEF;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            // Display the card details in the UI
            cardNameText.text = cardName;
            cardEffectText.text = cardEffect;
            cardSoulEnergyText.text = cardSoulEnergy;
            cardATKText.text = cardATK;
            cardDEFText.text = cardDEF;
        }

        /// <summary>
        /// Function to set the card values in the UI
        /// </summary>
        /// <param name="card">The card whose info we're getting</param>
        public void SetCard(Card card)
        {
            // Set the card being passed in
            selectedCard = card;

            // Get the card name
            cardName = selectedCard.cardName;

            // Get the card effect/lore
            cardEffect = selectedCard.cardEffect;

            // Get the card level
            cardSoulEnergy = "Soul Energy " + selectedCard.cardSoulEnergy.ToString();

            // Get the card ATK
            cardATK = selectedCard.cardATK.ToString();

            // Get the card DEF
            cardDEF = selectedCard.cardDEF.ToString();
        }

        /// <summary>
        /// Function to return to the action menu
        /// </summary>
        public void BackToActionMenu()
        {
            // If the player is viewing the deck
            if (GameManager.Instance.viewingDeck)
            {
                // Show the action window, setting the card and the index
                GameManager.Instance.gameUI.ShowActionWindowDeck(selectedCard, selectedCardIndex);
            }
            else if (GameManager.Instance.viewingGrave)
            {
                // Show the action window, setting the card and the index
                GameManager.Instance.gameUI.ShowActionWindowDeck(selectedCard, selectedCardIndex);
            }
            else
            {
                // Show the action window, setting the card and the index
                GameManager.Instance.gameUI.ShowActionWindow(selectedCard, selectedCardIndex);
            }

            // Hide the back button
            backButton.gameObject.SetActive(false);

            // Hide this window
            GameManager.Instance.gameUI.cardInfoWindow.SetActive(false);
        }
    }
}