﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NotPublic
{
	public class Deck : MonoBehaviour
	{
		/* Public Variables */
		public SpriteRenderer spriteRenderer;
		public Sprite cardBackSprite;
		public List<Card> deck;

		// Use this for initialization
		void Start()
		{
			spriteRenderer = GetComponent<SpriteRenderer>();
		}

		// Update is called once per frame
		void Update()
		{
			if (deck.Count > 0)
			{
				spriteRenderer.sprite = cardBackSprite;
			}
			else
			{
				spriteRenderer.sprite = null;
			}
		}
	}
}