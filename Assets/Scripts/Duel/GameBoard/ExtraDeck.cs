﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NotPublic
{
	public class ExtraDeck : MonoBehaviour
	{
		/* Public Variables */
		public SpriteRenderer spriteRenderer;
		public Sprite cardBackSprite;
		public List<Card> extraDeck;

		// Use this for initialization
		void Start()
		{
			spriteRenderer = GetComponent<SpriteRenderer>();
		}

		// Update is called once per frame
		void Update()
		{
			if (extraDeck.Count > 0)
			{
				spriteRenderer.sprite = cardBackSprite;
			}
			else
			{
				spriteRenderer.sprite = null;
			}
		}
	}
}