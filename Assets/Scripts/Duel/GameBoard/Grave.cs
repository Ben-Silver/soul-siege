﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NotPublic
{
	public class Grave : MonoBehaviour
	{
		/* Public Variables */
		public List<Card> grave;
		public Sprite topCardSprite;
		public SpriteRenderer sprite;

		// Use this for initialization
		void Start()
		{
			sprite = GetComponent<SpriteRenderer>();
		}

		// Update is called once per frame
		void Update()
		{
			if (grave.Count > 0)
			{
				topCardSprite = grave[grave.Count - 1].cardSprite;
				sprite.sprite = topCardSprite;
			}
		}
	}
}