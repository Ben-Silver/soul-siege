﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NotPublic
{
    public class CardZone : MonoBehaviour
    {
        /* Public Variables */
        public ZoneType zoneType;
        public Card cardInZone;
        public bool occupied;
        public SpriteRenderer spriteRenderer;
        public SpriteRenderer attackIcon;

        // Use this for initialization
        void Start()
        {
            if (zoneType != ZoneType.Grave)
            {
                spriteRenderer = GetComponent<SpriteRenderer>();
            }
        }

        // Update is called once per frame
        void Update()
        {
            occupied = (cardInZone != null) ? true : false;

            if (occupied)
            {
                spriteRenderer.sprite = cardInZone.cardSprite;
            }
            else
            {
                if (spriteRenderer != null)
                {
                    spriteRenderer.sprite = null;
                }
            }

            if (attackIcon != null && occupied)
            {
                if (GameManager.Instance.currentPhase == Phase.BattlePhase)
                {
                    if (GameManager.Instance.currentTurn == Turn.Player)
                    {
                        attackIcon.gameObject.SetActive(true);
                    }
                }
                else
                {
                    attackIcon.gameObject.SetActive(false);
                }
            }
        }

        void OnMouseDown()
        {
            if (GameManager.Instance.currentTurn == Turn.Player)
            {
                if (GameManager.Instance.currentPhase == Phase.BattlePhase)
                {
                    if (occupied)
                    {
                        GameManager.Instance.opponentLife -= cardInZone.cardATK;
                    }
                }
            }

            if (occupied)
            {
                GameManager.Instance.gameUI.cardInfoWindow.SetActive(true);
                GameManager.Instance.gameUI.cardInfoWindow.GetComponent<CardInfoWindow>().backButton.gameObject.SetActive(false);
                GameManager.Instance.gameUI.cardInfoWindow.GetComponent<CardInfoWindow>().SetCard(cardInZone);
            }
        }

        void OnMouseUp()
        {
            if (GameManager.Instance.gameUI.cardInfoWindow.activeInHierarchy)
            {
                GameManager.Instance.gameUI.cardInfoWindow.SetActive(false);
            }
        }
    }

    public enum ZoneType
    {
        Monster,
        Spell,
        Terrain,
        Deck,
        Grave
    }
}