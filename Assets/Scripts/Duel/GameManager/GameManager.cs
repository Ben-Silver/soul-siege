﻿using NotPublic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Manages everything in during the duel
/// </summary>
public class GameManager : Singleton<GameManager>
{
    [Header("Game Components")]
    public Deck mainDeck;
    public Hand hand;
    public Grave grave;
    public Deck mainDeckOpp;
    public Hand handOpp;
    public Grave graveOpp;
    public int turnCount = 0;
    public int handLimit = 7;
    public int playerLife = 2000;
    public int opponentLife = 2000;

    [Header("Partner Components")]
    public GameManagerUI gameUI;

    [Header("Turn Components")]
    public Turn currentTurn;
    public Phase currentPhase;
    public int numberOfSummons;
    public int soulEnergy;
    public bool viewingDeck;
    public bool viewingGrave;

    private int startingHandSize = 4;

    // Use this for initialization
    void Start()
    {
        DrawStartingHand();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerTurn();
        OpponentTurn();
    }

    /// <summary>
    /// Function to draw a card from a player's deck
    /// </summary>
    public void DrawCard(bool isPlayer)
    {
        if (isPlayer)
        {
            while (hand.hand.Count >= handLimit)
            {
                Discard(Random.Range(0, hand.hand.Count - 1), true);
            }

            int rand = Random.Range(0, mainDeck.deck.Count - 1);
            hand.hand.Add(mainDeck.deck[rand]);
            mainDeck.deck.RemoveAt(rand);
        }
        else
        {
            while (handOpp.hand.Count >= handLimit)
            {
                Discard(Random.Range(0, handOpp.hand.Count - 1), false);
            }

            int rand = Random.Range(0, mainDeckOpp.deck.Count - 1);
            handOpp.hand.Add(mainDeckOpp.deck[rand]);
            mainDeckOpp.deck.RemoveAt(rand);
        }
    }

    /// <summary>
    /// Function to discard a card from a player's hand
    /// </summary>
    /// <param name="indexNumber">The index of the card in the list</param>
    public void Discard(int indexNumber, bool isPlayer)
    {
        if (isPlayer)
        {
            grave.grave.Add(hand.hand[indexNumber]);
            hand.hand.RemoveAt(indexNumber);
        }
        else
        {
            graveOpp.grave.Add(handOpp.hand[indexNumber]);
            handOpp.hand.RemoveAt(indexNumber);
        }
    }

    /// <summary>
    /// Function to give each player their starting hand
    /// </summary>
    void DrawStartingHand()
    {
        for (int i = 0; i < startingHandSize; i++)
        {
            DrawCard(true);
            DrawCard(false);
        }

        SwitchPhase(Phase.DrawPhase);
    }

    /// <summary>
    /// Switches to the phase being passed
    /// </summary>
    public void SwitchPhase(Phase newPhase)
    {
        currentPhase = newPhase;
        gameUI.TogglePhasesWindow(false);
    }

    /// <summary>
    /// Switches to the phase being passed
    /// </summary>
    public void SwitchPhase(string newPhase)
    {
        currentPhase = (Phase)System.Enum.Parse(typeof(Phase), newPhase);
        gameUI.TogglePhasesWindow(false);
    }

    /// <summary>
    /// Function to update the player's turn
    /// </summary>
    public void PlayerTurn()
    {
        if (currentTurn == Turn.Player)
        {
            switch (currentPhase)
            {
                case (Phase.DrawPhase):

                    gameUI.currentPhaseText.text = "Draw Phase";
                    turnCount++;
                    DrawCard(true);
                    numberOfSummons = 1;
                    soulEnergy = 10;
                    gameUI.DisablePhaseButtons();
                    gameUI.EnablePhaseButtons(1);
                    currentPhase = Phase.StandbyPhase;

                    break;

                case (Phase.StandbyPhase):

                    gameUI.currentPhaseText.text = "Standby Phase";
                    currentPhase = Phase.MainPhase;

                    break;

                case (Phase.MainPhase):

                    gameUI.currentPhaseText.text = "Main Phase";
                    gameUI.DisablePhaseButtons();

                    if (turnCount > 1)
                    {
                        gameUI.EnablePhaseButtons(3);
                    }
                    else
                    {
                        gameUI.EnablePhaseButtons(4);
                    }

                    gameUI.phaseButton.SetActive(true);

                    break;

                case (Phase.BattlePhase):

                    gameUI.currentPhaseText.text = "Battle Phase";
                    gameUI.DisablePhaseButtons();
                    gameUI.EnablePhaseButtons(4);
                    gameUI.phaseButton.SetActive(true);

                    break;

                case (Phase.EndPhase):

                    gameUI.currentPhaseText.text = "End Phase";

                    for (int i = 0; i < gameUI.cardZones.Length; i++)
                    {
                        if (gameUI.cardZones[i].zoneType == ZoneType.Monster)
                        {
                            if (gameUI.cardZones[i].occupied)
                            {
                                gameUI.cardZones[i].cardInZone.battledThisTurn = false;
                            }
                        }
                    }

                    currentTurn = Turn.Opponent;
                    currentPhase = Phase.DrawPhase;
                    gameUI.DisablePhaseButtons();
                    gameUI.phaseButton.SetActive(false);

                    break;
            }
        }
    }

    /// <summary>
    /// Function to update the opponent's turn
    /// </summary>
    public void OpponentTurn()
    {
        if (currentTurn == Turn.Opponent)
        {
            switch (currentPhase)
            {
                case (Phase.DrawPhase):

                    gameUI.currentPhaseText.text = "Draw Phase";
                    DrawCard(false);
                    turnCount++;
                    numberOfSummons = 1;
                    soulEnergy = 10;
                    currentPhase = Phase.StandbyPhase;

                    break;

                case (Phase.StandbyPhase):

                    gameUI.currentPhaseText.text = "Standby Phase";
                    currentPhase = Phase.MainPhase;

                    break;

                case (Phase.MainPhase):

                    gameUI.currentPhaseText.text = "Main Phase";

                    for (int i = 0; i < handOpp.hand.Count; i++)
                    {
                        if (handOpp.hand[i].cardType == CardType.Monster)
                        {
                            if (numberOfSummons > 0 && soulEnergy > handOpp.hand[i].cardSoulEnergy)
                            {
                                NormalSummonOpp(handOpp.hand[i], i);
                                return;
                            }
                        }
                    }

                    if (turnCount > 1)
                    {
                        SwitchPhase(Phase.BattlePhase);
                    }

                    break;

                case (Phase.BattlePhase):

                    gameUI.currentPhaseText.text = "Battle Phase";
                    ConductBattlePhase();
                    SwitchPhase(Phase.EndPhase);

                    break;

                case (Phase.EndPhase):

                    gameUI.currentPhaseText.text = "End Phase";

                    for (int i = 0; i < gameUI.cardZonesOpp.Length; i++)
                    {
                        if (gameUI.cardZonesOpp[i].zoneType == ZoneType.Monster)
                        {
                            if (gameUI.cardZonesOpp[i].occupied)
                            {
                                gameUI.cardZonesOpp[i].cardInZone.battledThisTurn = false;
                            }
                        }
                    }

                    currentTurn = Turn.Player;
                    currentPhase = Phase.DrawPhase;

                    break;
            }
        }
    }

    /// <summary>
    /// Function to conduct the opponent's battle phase
    /// </summary>
    public void ConductBattlePhase()
    {
        for (int j = 0; j < gameUI.cardZonesOpp.Length; j++)
        {
            if (gameUI.cardZonesOpp[j].zoneType == ZoneType.Monster && gameUI.cardZonesOpp[j].occupied && !gameUI.cardZonesOpp[j].cardInZone.battledThisTurn)
            {
                for (int i = 0; i < gameUI.cardZones.Length; i++)
                {
                    if (gameUI.cardZones[i].zoneType == ZoneType.Monster && gameUI.cardZones[i].occupied)
                    {
                        if (gameUI.cardZonesOpp[j].cardInZone.cardATK > gameUI.cardZones[i].cardInZone.cardATK)
                        {
                            playerLife -= gameUI.cardZonesOpp[j].cardInZone.cardATK - gameUI.cardZones[i].cardInZone.cardATK;
                            gameUI.cardZonesOpp[j].cardInZone.battledThisTurn = true;
                            DestroyCard("Battle", true, gameUI.cardZones[i].cardInZone, i);
                        }
                        else if (gameUI.cardZonesOpp[j].cardInZone.cardATK == gameUI.cardZones[i].cardInZone.cardATK)
                        {
                            DestroyCard("Battle", true, gameUI.cardZones[i].cardInZone, i);
                            DestroyCard("Battle", false, gameUI.cardZonesOpp[j].cardInZone, j);
                        }
                        else
                        {
                            return;
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Function to destroy a card
    /// </summary>
    /// <param name="destructionMethod">How the card was destroyed</param>
    /// <param name="playerCard">True if the card belonged to the player</param>
    /// <param name="destroyedCard">The card that was destroyed</param>
    /// <param name="zoneIndex">Which zone the card was in</param>
    public void DestroyCard(string destructionMethod, bool playerCard, Card destroyedCard, int zoneIndex)
    {
        if (playerCard)
        {
            if (destructionMethod == "Battle")
            {
                gameUI.cardZones[zoneIndex].cardInZone = null;
                grave.grave.Add(destroyedCard);
            }
            else if (destructionMethod == "Effect")
            {
                gameUI.cardZones[zoneIndex].cardInZone = null;
                grave.grave.Add(destroyedCard);
            }
            else if (destructionMethod == "Sent")
            {
                gameUI.cardZones[zoneIndex].cardInZone = null;
                grave.grave.Add(destroyedCard);
            }
        }
        else
        {
            if (destructionMethod == "Battle")
            {
                gameUI.cardZonesOpp[zoneIndex].cardInZone = null;
                graveOpp.grave.Add(destroyedCard);
            }
            else if (destructionMethod == "Effect")
            {
                gameUI.cardZonesOpp[zoneIndex].cardInZone = null;
                graveOpp.grave.Add(destroyedCard);
            }
            else if (destructionMethod == "Sent")
            {
                gameUI.cardZonesOpp[zoneIndex].cardInZone = null;
                graveOpp.grave.Add(destroyedCard);
            }
        }
    }

    /// <summary>
    /// Function to normal summon a monster from the opponent's hand
    /// </summary>
    /// <param name="cardToSummon">The card in the opponent's hand that is being summoned</param>
    /// <param name="indexNumber">The array index of the card being summon</param>
    public void NormalSummonOpp(Card cardToSummon, int indexNumber)
    {
        for (int i = 0; i < gameUI.cardZonesOpp.Length; i++)
        {
            if (gameUI.cardZonesOpp[i].zoneType == ZoneType.Monster)
            {
                if (!gameUI.cardZonesOpp[i].occupied)
                {
                    gameUI.cardZonesOpp[i].cardInZone = cardToSummon;
                    handOpp.hand.RemoveAt(indexNumber);
                    numberOfSummons -= 1;
                    soulEnergy -= cardToSummon.cardSoulEnergy;

                    return;
                }
                else
                {
                    if (i >= 5)
                    {
                        currentPhase = (turnCount > 1) ? Phase.BattlePhase : Phase.EndPhase;
                        return;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Searches the deck for specific cards
    /// </summary>
    /// <param name="searchType"></param>
    /// <param name="searchParams"></param>
    public void SearchDeck(SearchType searchType, List<string> searchParams)
    {
        viewingDeck = true;
        gameUI.deckListWindow.gameObject.SetActive(true);

        for (int i = 0; i < mainDeck.deck.Count; i++)
        {
            for (int j = 0; j < searchParams.Count; j++)
            {
                switch (searchType)
                {
                    case SearchType.Name:

                        if (!mainDeck.deck[i].cardName.Contains(searchParams[j]))
                        {
                            gameUI.deckListWindow.parentedCards[i].SetActive(false);
                        }

                        break;

                    case SearchType.Type:

                        if (mainDeck.deck[i].monsterType != (MonsterType)System.Enum.Parse(typeof(MonsterType), searchParams[j]))
                        {
                            gameUI.deckListWindow.parentedCards[i].SetActive(false);
                        }

                        break;

                    case SearchType.ATK:

                        if (mainDeck.deck[i].cardATK != int.Parse(searchParams[j]))
                        {
                            gameUI.deckListWindow.parentedCards[i].SetActive(false);
                        }

                        break;

                    case SearchType.DEF:

                        if (mainDeck.deck[i].cardDEF != int.Parse(searchParams[j]))
                        {
                            gameUI.deckListWindow.parentedCards[i].SetActive(false);
                        }

                        break;

                    case SearchType.Level:

                        if (mainDeck.deck[i].cardSoulEnergy != int.Parse(searchParams[j]))
                        {
                            gameUI.deckListWindow.parentedCards[i].SetActive(false);
                        }

                        break;
                }
            }
        }
    }
}

namespace NotPublic
{
    public enum Turn
    {
        Player,
        Opponent
    }

    public enum Phase
    {
        DrawPhase,
        StandbyPhase,
        MainPhase,
        BattlePhase,
        EndPhase
    }

    public enum SearchType
    {
        Name,
        Type,
        ATK,
        DEF,
        Level
    }
}