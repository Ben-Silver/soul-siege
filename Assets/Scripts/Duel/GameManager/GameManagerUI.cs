﻿using NotPublic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/// <summary>
/// Manages all the UI components for the GameManager
/// </summary>
public class GameManagerUI : MonoBehaviour
{
    [Header("Partner Components")]
    public GameManager gameManager;

    [Header("UI Components")]
    public Text playerLifeText;
    public Text opponentLifeText;
    public Text turnCountText;
    public Text currentTurnText;
    public Text currentPhaseText;
    public CardZone[] cardZones;
    public CardZone[] cardZonesOpp;
    public GameObject phaseButton;
    public GameObject phaseWindow;
    public Button[] changePhaseButton;
    public GameObject cardInfoWindow;
    public CardActionWindow cardActionWindow;
    public DeckListWindow deckListWindow;


    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.Instance;
    }

    // Update is called once per frame
    void Update()
    {
        currentTurnText.text = (gameManager.currentTurn == Turn.Player) ? "Player's Turn" : "Opponent's Turn";
        playerLifeText.text = "Player: " + gameManager.playerLife;
        opponentLifeText.text = "Opponent: " + gameManager.opponentLife;
        turnCountText.text = "Turn " + gameManager.turnCount;
    }

    /// <summary>
    /// Switches the phase window on/off
    /// </summary>
    public void TogglePhasesWindow(bool isOn)
    {
        phaseButton.SetActive(!isOn);
        phaseWindow.SetActive(isOn);
    }

    /// <summary>
    /// Function to disable all the buttons in the phase window
    /// </summary>
    public void DisablePhaseButtons()
    {
        for (int i = 0; i < changePhaseButton.Length; i++)
        {
            changePhaseButton[i].interactable = false;
        }
    }

    /// <summary>
    /// Function to enable the phase buttons from a certain index
    /// </summary>
    /// <param name="startPoint">The starting index</param>
    public void EnablePhaseButtons(int startPoint)
    {
        for (int i = startPoint; i < changePhaseButton.Length; i++)
        {
            EnablePhaseButton(i.ToString());
        }
    }

    /// <summary>
    /// Function to enable a phase button
    /// </summary>
    /// <param name="phase">The string value of the button</param>
    public void EnablePhaseButton(string phase)
    {
        if (phase.Equals("Draw") || phase.Equals("0"))
        {
            changePhaseButton[0].interactable = true;
        }

        if (phase.Equals("Standby") || phase.Equals("1"))
        {
            changePhaseButton[1].interactable = true;
        }

        if (phase.Equals("Main") || phase.Equals("2"))
        {
            changePhaseButton[2].interactable = true;
        }

        if (phase.Equals("Battle") || phase.Equals("3"))
        {
            changePhaseButton[3].interactable = true;
        }

        if (phase.Equals("End") || phase.Equals("4"))
        {
            changePhaseButton[4].interactable = true;
        }
    }

    /// <summary>
    /// Function to show the action window when a card is selected
    /// </summary>
    /// <param name="selectedCard">The card being selected</param>
    public void ShowActionWindow(Card selectedCard, int selectedCardIndex)
    {
        cardInfoWindow.SetActive(false);
        cardActionWindow.selectedCardIndex = selectedCardIndex;
        cardActionWindow.selectedCard = selectedCard;
        cardActionWindow.selectedCardText.text = "What will you do with " + selectedCard.cardName;
        cardActionWindow.gameObject.SetActive(true);
    }

    /// <summary>
    /// Function to show the action window when a card is selected while viewing the deck or graveyard
    /// </summary>
    /// <param name="selectedCard">The card being selected</param>
    public void ShowActionWindowDeck(Card selectedCard, int selectedCardIndex)
    {
        cardInfoWindow.SetActive(false);
        deckListWindow.cardActionWindowDeck.selectedCardIndex = selectedCardIndex;
        deckListWindow.cardActionWindowDeck.selectedCard = selectedCard;
        deckListWindow.cardActionWindowDeck.selectedCardText.text = "What will you do with " + selectedCard.cardName;
        deckListWindow.cardActionWindowDeck.gameObject.SetActive(true);
    }
}
