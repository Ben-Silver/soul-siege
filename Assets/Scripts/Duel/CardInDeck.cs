﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NotPublic
{
	public class CardInDeck : MonoBehaviour
	{
		public Image image;
		public Sprite blankDefault;
		public Card card;
		public int indexNumber;

		private int indexOutput = 0;

		// Use this for initialization
		void Start()
		{
			image = GetComponent<Image>();
			indexNumber = (int.TryParse(gameObject.name, out indexOutput)) ? indexOutput : -1;
		}

		// Update is called once per frame
		void Update()
		{
			if (indexNumber > -1 && indexNumber < GameManager.Instance.mainDeck.deck.Count)
			{
				card = GameManager.Instance.mainDeck.deck[indexNumber];

				if (card.cardSprite != null)
				{
					image.sprite = card.cardSprite;
				}
				else
				{
					image.sprite = blankDefault;
				}
			}
			else
			{
				gameObject.SetActive(false);
			}
		}

		/// <summary>
		/// Function to be called when the card is clicked/tapped
		/// </summary>
		public void OnCardTapped()
		{
			if (indexNumber > -1)
			{
				GameManager.Instance.gameUI.ShowActionWindowDeck(card, indexNumber);
			}
		}
	}
}