﻿using NotPublic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NotPublic
{
    public class Card : ScriptableObject
    {
        [Header("Card Details")]
        public string cardID;
        public string cardName;
        public CardType cardType;
        public MonsterType monsterType;
        public int cardSoulEnergy;
        public int cardATK;
        public int cardDEF;
        public string cardEffect;
        public Sprite cardSprite;

        [Header("Card Details")]
        public Rarity cardRarity;
        public int cardQuantity;
        public bool battledThisTurn;

        [Header("Card Effects")]
        public EffectType effectType;
        public int numberToDraw;
        public int numberToSummon;
        public SearchType searchType;
        public List<string> searchParams;

        public void Initialise(MockCard _mockCard)
        {
            name = _mockCard.cardID;
            cardID = _mockCard.cardID;
            cardName = _mockCard.cardName;
            cardType = _mockCard.cardType;
            monsterType = _mockCard.monsterType;
            cardSoulEnergy = _mockCard.cardSoulEnergy;
            cardATK = _mockCard.cardATK;
            cardDEF = _mockCard.cardDEF;
            cardEffect = _mockCard.cardEffect;
            cardSprite = _mockCard.cardSprite;
            cardRarity = _mockCard.cardRarity;
            cardQuantity = _mockCard.cardQuantity;
            battledThisTurn = _mockCard.battledThisTurn;
            effectType = _mockCard.effectType;
            numberToDraw = _mockCard.numberToDraw;
            numberToSummon = _mockCard.numberToSummon;
            searchType = _mockCard.searchType;
            searchParams = _mockCard.searchParams;
        }

        /// <summary>
        /// Activates the card effect
        /// </summary>
        public void ActivateEffect()
        {
            if (effectType == EffectType.Draw)
            {
                for (int i = 0; i < numberToDraw; i++)
                {
                    GameManager.Instance.DrawCard(true);
                }
            }
            else if (effectType == EffectType.Search)
            {
                GameManager.Instance.SearchDeck(searchType, searchParams);
                //GameManager.Instance.viewingDeck = true;
                //GameManager.Instance.deckListWindow.gameObject.SetActive(true);
            }
            else if (effectType == EffectType.DestroyMonster)
            {
            }
            else if (effectType == EffectType.SpecialSummonHand)
            {
            }
            else if (effectType == EffectType.SpecialSummonGrave)
            {
            }

            // If the effect was a spell effect, we want it to go to the grave after
            if (cardType == CardType.Spell)
            {
                SendToGrave();
            }
        }

        /// <summary>
        /// Sends the card to the grave after activation
        /// </summary>
        public void SendToGrave()
        {
            for (int i = 0; i < GameManager.Instance.gameUI.cardZones.Length; i++)
            {
                if (!GameManager.Instance.viewingDeck && !GameManager.Instance.viewingGrave)
                {
                    if (GameManager.Instance.gameUI.cardZones[i].zoneType == ZoneType.Spell)
                    {
                        if (GameManager.Instance.gameUI.cardZones[i].cardInZone.cardName == this.cardName)
                        {
                            GameManager.Instance.grave.grave.Add(GameManager.Instance.gameUI.cardZones[i].cardInZone);
                            GameManager.Instance.gameUI.cardZones[i].cardInZone = null;

                            return;
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// The card type
    /// </summary>
    public enum CardType
    {
        Monster,
        Spell
    }

    /// <summary>
    /// The type of monster
    /// </summary>
    public enum MonsterType
    {
        Beast,
        Dragon,
        Fiend,
        Machine,
        SeaSerpent,
        Warrior,
        WingedBeast,
        Spell,          // For Spell cards or spell searchers
        None,
    }

    /// <summary>
    /// The possible effect types
    /// </summary>
    public enum EffectType
    {
        None,
        Draw,
        Search,
        DestroyMonster,
        SpecialSummonHand,
        SpecialSummonGrave
    }

    /// <summary>
    /// The possible card rarities
    /// </summary>
    public enum Rarity
    {
        Normal,
        Rare,
        SuperRare,
        UltraRare
    }
}