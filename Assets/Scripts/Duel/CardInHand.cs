﻿using NotPublic;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardInHand : MonoBehaviour
{
	public Image image;
	public Sprite blankDefault;
	public Card card;
	public int indexNumber;

	private int indexOutput = 0;

	// Use this for initialization
	void Start()
	{
		image = GetComponent<Image>();
		indexNumber = (int.TryParse(gameObject.name, out indexOutput)) ? indexOutput : -1;
	}
	
	// Update is called once per frame
	void Update()
	{
		if (indexNumber > -1 && indexNumber < GameManager.Instance.hand.hand.Count)
		{
			card = GameManager.Instance.hand.hand[indexNumber];

			if (card.cardSprite != null)
			{
				image.sprite = card.cardSprite;
			}
			else
			{
				image.sprite = blankDefault;
			}
		}
	}

    /// <summary>
    /// Function to be called when the card is clicked/tapped
    /// </summary>
	public void OnCardTapped()
	{
		if (GameManager.Instance.currentTurn == Turn.Player && GameManager.Instance.currentPhase == Phase.MainPhase)
		{
			if (indexNumber > -1)
			{
                GameManager.Instance.gameUI.ShowActionWindow(card, indexNumber);
				//PlayCard();
			}
		}
	}

    /// <summary>
    /// Function to play a card from the hand
    /// </summary>
	public void PlayCard()
	{
		if (card.cardType == CardType.Monster && GameManager.Instance.numberOfSummons > 0 && GameManager.Instance.soulEnergy > card.cardSoulEnergy)
		{
			for (int i = 0; i < GameManager.Instance.gameUI.cardZones.Length; i++)
			{
				if (GameManager.Instance.gameUI.cardZones[i].zoneType == ZoneType.Monster)
				{
					if (!GameManager.Instance.gameUI.cardZones[i].occupied)
					{
						GameManager.Instance.gameUI.cardZones[i].cardInZone = card;
						GameManager.Instance.hand.hand.RemoveAt(indexNumber);
						GameManager.Instance.numberOfSummons -= 1;
                        GameManager.Instance.soulEnergy -= card.cardSoulEnergy;

                        return;
					}
				}
			}
		}
		else if (card.cardType == CardType.Spell)
		{
			for (int i = 0; i < GameManager.Instance.gameUI.cardZones.Length; i++)
			{
				if (GameManager.Instance.gameUI.cardZones[i].zoneType == ZoneType.Spell)
				{
					if (!GameManager.Instance.gameUI.cardZones[i].occupied)
					{
						GameManager.Instance.gameUI.cardZones[i].cardInZone = card;
						GameManager.Instance.hand.hand.RemoveAt(indexNumber);

						return;
					}
				}
			}
		}
	}

	public void Discard()
	{
		GameManager.Instance.grave.grave.Add(GameManager.Instance.hand.hand[indexNumber]);
		GameManager.Instance.hand.hand.RemoveAt(indexNumber);
	}
}
