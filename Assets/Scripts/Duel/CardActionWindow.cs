﻿using NotPublic;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardActionWindow : MonoBehaviour
{
    public Card selectedCard;
    public Text selectedCardText;
    public int selectedCardIndex;
    public Button playButton;
	
	// Update is called once per frame
	void Update()
    {
		//selectedCardText.text = "What will you do with " + selectedCard.cardName;
        playButton.interactable = (GameManager.Instance.numberOfSummons < 1 && selectedCard.cardType == CardType.Monster) ? false : true;
	}

    /// <summary>
    /// Function to play the selected card
    /// </summary>
    public void PlayCard()
	{
		if (selectedCard.cardType == CardType.Monster && GameManager.Instance.numberOfSummons > 0 && GameManager.Instance.soulEnergy > selectedCard.cardSoulEnergy)
		{
			for (int i = 0; i < GameManager.Instance.gameUI.cardZones.Length; i++)
			{
				if (GameManager.Instance.gameUI.cardZones[i].zoneType == ZoneType.Monster && !GameManager.Instance.gameUI.cardZones[i].occupied)
				{
					GameManager.Instance.gameUI.cardZones[i].cardInZone = selectedCard;
					GameManager.Instance.hand.hand.RemoveAt(selectedCardIndex);
					GameManager.Instance.numberOfSummons -= 1;
					GameManager.Instance.soulEnergy -= selectedCard.cardSoulEnergy;
                    HideActionWindow();

					return;
				}
			}
		}
		else if (selectedCard.cardType == CardType.Spell)
		{
			for (int i = 0; i < GameManager.Instance.gameUI.cardZones.Length; i++)
			{
				if (GameManager.Instance.gameUI.cardZones[i].zoneType == ZoneType.Spell)
				{
					if (!GameManager.Instance.gameUI.cardZones[i].occupied)
					{
						GameManager.Instance.gameUI.cardZones[i].cardInZone = selectedCard;
						GameManager.Instance.hand.hand.RemoveAt(selectedCardIndex);
                        HideActionWindow();
                        GameManager.Instance.gameUI.cardZones[i].cardInZone.ActivateEffect();

						return;
					}
				}
			}
		}
	}

    /// <summary>
    /// Function to add the selected card from deck to hand
    /// </summary>
    public void AddCardFromDeckToHand()
    {
        GameManager.Instance.hand.hand.Add(selectedCard);
        GameManager.Instance.mainDeck.deck.RemoveAt(selectedCardIndex);
        HideActionWindow();
        GameManager.Instance.gameUI.deckListWindow.gameObject.SetActive(false);
        GameManager.Instance.viewingDeck = false;

        for (int i = 0; i < GameManager.Instance.gameUI.cardZones.Length; i++)
        {
            if (GameManager.Instance.gameUI.cardZones[i].zoneType == ZoneType.Spell && GameManager.Instance.gameUI.cardZones[i].occupied)
            {
                GameManager.Instance.gameUI.cardZones[i].cardInZone.SendToGrave();
            }
        }
    }

    /// <summary>
    /// Function to show the card info of the card being selected
    /// </summary>
    public void ShowCardInfo()
    {
        GameManager.Instance.gameUI.cardInfoWindow.GetComponent<CardInfoWindow>().backButton.gameObject.SetActive(true);
        GameManager.Instance.gameUI.cardInfoWindow.GetComponent<CardInfoWindow>().selectedCardIndex = selectedCardIndex;
        GameManager.Instance.gameUI.cardInfoWindow.SetActive(true);
        GameManager.Instance.gameUI.cardInfoWindow.GetComponent<CardInfoWindow>().SetCard(selectedCard);
        HideActionWindow();
    }
    
    /// <summary>
    /// Function to hide the action window
    /// </summary>
    public void HideActionWindow()
    {
        gameObject.SetActive(false);
    }
}
