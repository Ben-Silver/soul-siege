﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StringGenerator
{
    private static char[] chars =
    {
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };

    /// <summary>
    /// Return a string of random letters & numbers 
    /// </summary>
    public static string GenerateID(int _length)
    {
        string id = "";

        for (int i = 0; i < _length; i++)
        {
            id += chars[Random.Range(0, chars.Length)];
        }

        return id;
    }
}
