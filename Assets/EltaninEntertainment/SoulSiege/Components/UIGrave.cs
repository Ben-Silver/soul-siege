﻿using EltaninEntertainment.SoulSiege.Components.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EltaninEntertainment.SoulSiege.Components
{
	public class UIGrave : MonoBehaviour
	{
		/* Public Variables */
		public Image image;
		public Sprite topCardImage;
		public List<Card> grave;

		// Use this for initialization
		void Start()
		{
			image = GetComponent<Image>();
		}

		// Update is called once per frame
		void Update()
		{
			if (grave.Count > 0)
			{
				topCardImage = grave[grave.Count - 1].image;
				image.sprite = topCardImage;
			}
		}
	}
}