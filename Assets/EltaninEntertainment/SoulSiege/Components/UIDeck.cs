﻿using EltaninEntertainment.SoulSiege.Components.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace EltaninEntertainment.SoulSiege.Components
{
	public class UIDeck : MonoBehaviour
	{
        #region Variables

        [Header("GUI")]
		public Image image;
		public Sprite cardBackSprite;
		public TextMeshProUGUI deckSizeText;

		[Header("Deck List")]
		public Deck deckList;
		public List<Card> deck;

        #endregion

        #region Events

        public delegate void DeckSizeChanged();
		public DeckSizeChanged OnDeckSizeChanged;

        #endregion

        #region Initialisation

        void Start()
		{
			// Get the deck list and assign it to the list
			deck = new List<Card>(deckList.deck);		// <-- This method means that we won't affect the ScriptableObject assigned to deckList
		}

		private void OnEnable()
		{
			if (!image) { image = GetComponent<Image>(); }
			OnDeckSizeChanged += UpdateDeckSize;
			OnDeckSizeChanged?.Invoke();
		}

		private void OnDisable()
		{
			try { OnDeckSizeChanged -= UpdateDeckSize; } catch { }
		}

		#endregion

		private void UpdateDeckSize()
		{
			deckSizeText.text = deck.Count.ToString();
			if (deck.Count > 0)
			{
				image.sprite = cardBackSprite;
			}
			else
			{
				image.sprite = null;
			}
		}
	}
}