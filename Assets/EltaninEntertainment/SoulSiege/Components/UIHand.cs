﻿using EltaninEntertainment.SoulSiege.Components.Cards;
using EltaninEntertainment.SoulSiege.Components.Interactables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EltaninEntertainment.SoulSiege.Components
{
	public class UIHand : MonoBehaviour
	{
		[Header("Hand")]
		//public List<Card> hand;
		public List<UICard> handSlots;
		public bool showHand = true;

		[Header("Prefab")]
		public UICard UICardPrefab;
		
        #region Events

        public delegate void CardAdded(Card _card);
		public CardAdded OnCardAdded;

		public delegate void CardRemoved(UICard _card);
		public CardRemoved OnCardRemoved;

        #endregion

        #region Initialisation

        private void OnEnable()
		{
			OnCardAdded += AddCard;
			OnCardRemoved += RemoveCard;
		}

		private void OnDisable()
		{
			try { OnCardAdded -= AddCard; } catch { }
			try { OnCardRemoved -= RemoveCard; } catch { }
		}

        #endregion

        #region Hand Management

		//public void SetCardIndex()
		//{
		//	for (int i = 0; i < handSlots.Count; i++)
		//	{
		//		handSlots[i].cardIndex = i;
		//	}
		//}

        /// <summary>
        /// Subscribes to OnCardAdded and triggers whenever a card is added to the hand
        /// </summary>
        private void AddCard(Card _card)
		{
			// Create a new UICard and pass the details of the new card into it
			UICard newCard = Instantiate(_card.cardPrefab /*UICardPrefab*/, transform);
			newCard.IsInteractable = showHand;
			newCard.SetCard(_card);
			handSlots.Add(newCard);
			//newCard.cardIndex = handSlots.Count - 1;
			newCard.gameObject.name = handSlots.Count.ToString();
		}
		
		/// <summary>
		/// Subscribes to OnCardRemoved and triggers whenever a card is removed from the hand
		/// </summary>
		private void RemoveCard(UICard _card)
		{
			// Removed and destroy the UICard
			handSlots.Remove(_card);
			//Destroy(_card.gameObject);
		}

        #endregion
    }
}