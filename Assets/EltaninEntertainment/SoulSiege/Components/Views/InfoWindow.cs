﻿using EltaninEntertainment.Commons.Battle;
using EltaninEntertainment.SoulSiege.Components.Cards;
using EltaninEntertainment.SoulSiege.Components.Interactables;
using UnityEngine;
using UnityEngine.UI;

namespace EltaninEntertainment.SoulSiege.Components.Views
{
    public class InfoWindow : MonoBehaviour
    {
        [Header("Card Info")]
        public UICard selectedCard;
        //public int selectedCardIndex;

        [Header("Interface Image")]
        public Image cardImage;

        [Header("Buttons")]
        public Button SummonButton;
        public Button ActivateButton;
        public Button AttackButton;
        public Button AddButton;

        [Header("Back Button")]
        public Button backButton;

        [SerializeField] private string instanceId;
        /// <summary>
        /// <br>Remains unique to the gameObject.</br>
        /// <br>Necessary because Unity cannot differentiate between 2+ of the same ScriptableObjects.</br>
        /// </summary>
        public string InstanceId
        {
            get { return instanceId; }
            set { instanceId = value; }
        }

        public virtual void SetCard(UICard _card)
        { }
        
        public virtual void ActivateCard()
        { }

        /// <summary>
        /// Function to return to the action menu
        /// </summary>
        public virtual void BackToActionMenu()
        {
            //backButton.gameObject.SetActive(false);
            TurnBasedController.Instance.viewingDeck = false;

            gameObject.SetActive(false);
        }
    }
}