﻿using EltaninEntertainment.Commons.Battle;
using EltaninEntertainment.SoulSiege.Components.Cards;
using EltaninEntertainment.SoulSiege.Components.Interactables;
using EltaninEntertainment.SoulSiege.Enums.Cards;
using UnityEngine;
using TMPro;

namespace EltaninEntertainment.SoulSiege.Components.Views
{
    public class CardInfoWindow : InfoWindow
    {
        #region Variables

        [Header("Interface Text")]
        public TextMeshProUGUI cardNameText;
        public TextMeshProUGUI cardAbilityText;
        public TextMeshProUGUI cardSoulEnergyText;
        public TextMeshProUGUI cardStrengthText;
        public TextMeshProUGUI cardStaminaText;

        #endregion

        #region Initialisation

        /// <summary>
        /// Function to set the card values in the UI
        /// </summary>
        /// <param name="_card">The card whose info we're getting</param>
        public override void SetCard(UICard _card)
        {
            selectedCard = _card;

            cardNameText.text = selectedCard.cardInfo.cardName;
            cardAbilityText.text = selectedCard.cardInfo.ability.text;

            cardImage.sprite = selectedCard.cardImage.sprite;

            int numberOfSummons = TurnBasedController.Instance.numberOfSummons;
            bool isPlayerTurn = TurnBasedController.Instance.IsPlayerTurn();

            bool isOnField = _card.transform.parent.GetComponent<UICardZone>();

            SummonButton.interactable = ((selectedCard.cardInfo.cardType == CardType.Unit && numberOfSummons > 0) || (selectedCard.cardInfo.cardType == CardType.Tactic)) && isPlayerTurn;
            ActivateButton.interactable = isOnField;
        }

        public override void ActivateCard()
        {
            Debug.Log("Activating card");
            StartCoroutine(selectedCard.GetComponent<Commons.Cards.Card>()
                                       .ExcecuteCardEffect());

            BackToActionMenu();
        }

        #endregion

        /// <summary>
        /// Function to return to the action menu
        /// </summary>
        public override void BackToActionMenu()
        {
            base.BackToActionMenu();
        }
    }
}
