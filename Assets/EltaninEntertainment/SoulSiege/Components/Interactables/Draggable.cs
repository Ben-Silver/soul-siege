﻿using EltaninEntertainment.Commons.Battle;
using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace EltaninEntertainment.SoulSiege.Components.Interactables
{
    public class Draggable : Interactable, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        #region Variables
        [Header("GUI Settings")]
        [SerializeField] protected CanvasGroup canvasGroup;
        [SerializeField] protected Transform returnArea;
        [SerializeField] protected Transform placeholderReturnArea;
        GameObject placeholder = null;

        [Header("Held Settings")]
        public float smoothDragSpeed = 4;

        [SerializeField] protected RectTransform rectTransform;
        [SerializeField] protected Vector3 initialPosition;

        [SerializeField] protected PointerEventData pointerData;
        private Vector3 vel = Vector3.zero;

        protected Transform oldReturnArea = null;

        #endregion

        #region Initialisation

        // Start is called before the first frame update
        protected virtual void Start() { }

        #endregion

        #region Updating

        private void Update()
        {
            if (dragging)
            {
                transform.position = Vector3.SmoothDamp(transform.position, pointerData.position, ref vel, 0.1f);
            }
        }

        private IEnumerator ReturnToPosition(Vector3 _position)
        {
            while (rectTransform.position != _position && !dragging)
            {
                rectTransform.position = Vector3.SmoothDamp(rectTransform.position, _position, ref vel, 0.1f);
                yield return new WaitForEndOfFrame();
            }
            yield return null;
        }

        #endregion

        #region Dragging

        public virtual void OnBeginDrag(PointerEventData _data)
        {
            if (!IsInteractable || TurnBasedController.Instance.viewingDeck) { return; }

            CreatePlaceholder();

            returnArea = transform.parent;
            placeholderReturnArea = returnArea;
            transform.SetParent(transform.parent.parent);

            dragging = true;
            pointerData = _data;
            canvasGroup.blocksRaycasts = false;
        }

        public virtual void OnEndDrag(PointerEventData _data)
        {
            if (!IsInteractable || TurnBasedController.Instance.viewingDeck) { return; }

            dragging = false;
            pointerData = _data;
            
            transform.SetParent(returnArea);
            canvasGroup.blocksRaycasts = true;

            transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());

            Destroy(placeholder);

            //StartCoroutine(ReturnToPosition(initialPosition));
        }

        public virtual void OnDrag(PointerEventData _data)
        {
            int newIndex = placeholderReturnArea.childCount;

            if (placeholder.transform.parent != placeholderReturnArea)
            {
                placeholder.transform.SetParent(placeholderReturnArea);
            }

            for (int i = 0; i < placeholderReturnArea.childCount; i++)
            {
                if (transform.position.x < placeholderReturnArea.GetChild(i).position.x)
                {
                    newIndex = i;

                    if (placeholder.transform.GetSiblingIndex() < newIndex)
                    {
                        newIndex--;
                    }

                    break;
                }
            }
            
            placeholder.transform.SetSiblingIndex(newIndex);
        }

        private void CreatePlaceholder()
        {
            placeholder = new GameObject();
            placeholder.transform.SetParent(transform.parent);
            placeholder.transform.localScale = Vector3.one;
            
            LayoutElement layoutElement = placeholder.AddComponent<LayoutElement>();
            layoutElement.preferredWidth = GetComponent<LayoutElement>().preferredWidth;
            layoutElement.preferredHeight = GetComponent<LayoutElement>().preferredHeight;
            layoutElement.flexibleWidth = 0;
            layoutElement.flexibleHeight = 0;

            RectTransform rectTransform = placeholder.GetComponent<RectTransform>();
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, layoutElement.preferredWidth);
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, layoutElement.preferredHeight);

            placeholder.transform.SetSiblingIndex(transform.GetSiblingIndex());
        }

        #endregion

        #region Accessors and Mutators

        public virtual void DroppedInZone(Transform _returnArea)
        {
            Debug.LogFormat("Dropped in a zone, {0}", _returnArea.gameObject.name);
            oldReturnArea = returnArea;

            returnArea = _returnArea;
        }

        public virtual void EnteredDropZone(Transform _proxyReturnArea)
        {
            placeholderReturnArea = _proxyReturnArea;
        }
        
        public virtual void ExitedDropZone()
        {
            placeholderReturnArea = returnArea;
        }

        public virtual Transform GetReturnArea()
        {
            return returnArea;
        }

        #endregion

        void OnValidate()
        {
            if (!rectTransform) { rectTransform = GetComponent<RectTransform>(); }
        }
    }
}