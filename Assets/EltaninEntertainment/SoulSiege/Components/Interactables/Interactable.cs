﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace EltaninEntertainment.SoulSiege.Components.Interactables
{
    public class Interactable : MonoBehaviour, IPointerClickHandler
    {
        #region Variables

        [Header("Interactability")]
        [SerializeField] private bool isInteractable = true;
        public bool IsInteractable
        {
            get
            {
                return isInteractable;
            }
            set
            {
                if (isInteractable != value)
                {
                    OnInteractableChanged?.Invoke(value);
                }
                isInteractable = value;
            }
        }

        /// <summary>
        /// Is the interactable being dragged currently?
        /// </summary>
        public bool dragging;

        #endregion

        public virtual void Update()
        {
            
        }

        #region Events

        public delegate void InteractableChanged(bool _interactable);
        /// <summary>
        /// If this interactive object becomes usable again, this fires!
        /// </summary>
        public event InteractableChanged OnInteractableChanged;

        #endregion

        #region Clicking

        public virtual void OnPointerClick(PointerEventData _data)
        {
            if (!IsInteractable) { return; }
        }

        #endregion
    }
}