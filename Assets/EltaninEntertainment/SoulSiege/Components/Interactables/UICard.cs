﻿using EltaninEntertainment.SoulSiege.Components.Cards;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using EltaninEntertainment.Commons.Battle;
using EltaninEntertainment.SoulSiege.Enums.Zones;

namespace EltaninEntertainment.SoulSiege.Components.Interactables
{
    public class UICard : Draggable
    {
        #region Variables

        [Header("Card Settings")]
        public Card cardInfo;
        public Image cardImage;
        public Sprite cardBack;
        public List<Armoury> attachedArmoury;
        public List<SoulEnergy> attachedSoulEnergy;
        //public int cardIndex;

        [SerializeField] private string instanceId;
        /// <summary>
        /// <br>Remains unique to the gameObject.</br>
        /// <br>Necessary because Unity cannot differentiate between 2+ of the same ScriptableObjects.</br>
        /// </summary>
        public string InstanceId
        {
            get { return instanceId; }
            set { instanceId = value; }
        }

        #endregion

        public delegate void CardChanged();
        public CardChanged OnCardChanged;

        #region Initialisation

        protected override void Start()
        {
            base.Start();

            initialPosition = rectTransform.position;
            if (cardInfo) { SetCard(cardInfo); }

            InstanceId = StringGenerator.GenerateID(8);
        }

        public void SetCard(Card _cardInfo)
        {
            cardInfo = _cardInfo;
            gameObject.name = cardInfo.cardName; // _cardInfo != null ? cardInfo.cardName : cardIndex.ToString();
            cardImage.sprite = IsInteractable ? _cardInfo.image : cardBack;
        }

        #endregion

        #region Clicking

        public override void OnEndDrag(PointerEventData _data)
        {
            Debug.LogFormat("base.OnEndDrag");
            base.OnEndDrag(_data);

            Debug.LogFormat("Dropped in a zone");
            UICardZone zone = returnArea.GetComponent<UICardZone>();

            if (zone == null || zone.zoneType != ZoneType.Unit || zone.Occupied)
            {
                returnArea = oldReturnArea;
                oldReturnArea = null;

                return;
            }

            TurnBasedController.Instance.PlaceCard(this, zone);
        }

        #endregion

        #region Clicking

        public override void OnPointerClick(PointerEventData _data)
        {
            base.OnPointerClick(_data);

            if (!dragging && IsInteractable && !TurnBasedController.Instance.viewingDeck)
            {
                // Get the card info
                TurnBasedController.Instance.ShowInfoWindow(0, this);
            }
        }

        #endregion
    }
}