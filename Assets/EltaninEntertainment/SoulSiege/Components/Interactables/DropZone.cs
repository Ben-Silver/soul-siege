﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace EltaninEntertainment.SoulSiege.Components.Interactables
{
    public class DropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
    {
        #region Variables

        [SerializeField] private bool isInteractable = true;
        public bool IsInteractable
        {
            get
            {
                return isInteractable;
            }
            set
            {
                if (isInteractable != value)
                {
                    OnInteractableChanged?.Invoke(value);
                }
                isInteractable = value;
            }
        }

        #endregion

        #region Events

        public delegate void InteractableChanged(bool _interactable);
        /// <summary>
        /// If this interactive object becomes usable again, this fires!
        /// </summary>
        public event InteractableChanged OnInteractableChanged;

        #endregion

        #region Interacting

        public virtual void OnPointerEnter(PointerEventData _data)
        {
            if (!_data.pointerDrag) { return; }

            Draggable draggable = _data.pointerDrag.GetComponent<Draggable>();

            if (draggable)
            {
                draggable.EnteredDropZone(transform);
            }
        }

        public virtual void OnPointerExit(PointerEventData _data)
        {
            if (!_data.pointerDrag) { return; }

            Draggable draggable = _data.pointerDrag.GetComponent<Draggable>();

            if (draggable && draggable.GetReturnArea() == transform)
            {
                draggable.ExitedDropZone();
            }
        }

        public virtual void OnDrop(PointerEventData _data)
        {
            Draggable draggable = _data.pointerDrag.GetComponent<Draggable>();

            if (draggable)
            {
                draggable.DroppedInZone(transform);
            }
        }

        #endregion
    }
}