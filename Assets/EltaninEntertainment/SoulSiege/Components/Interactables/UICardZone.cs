﻿using EltaninEntertainment.SoulSiege.Enums.Cards;
using EltaninEntertainment.SoulSiege.Enums.Zones;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using EltaninEntertainment.Commons.Battle;

namespace EltaninEntertainment.SoulSiege.Components.Interactables
{
    public class UICardZone : DropZone, IPointerClickHandler
    {
        #region Variables

        [Header("Zone Type")]
        public ZoneType zoneType;
        public bool isPlayerZone;

        [Header("Zone Info")]
        [SerializeField] private UICard cardInZone;
        public UICard CardInZone
        {
            get { return cardInZone; }
            set
            {
                cardInZone = value;
                Occupied = cardInZone != null;

                if (cardInZone != null && cardInZone.cardInfo.cardType == CardType.Tactic)
                {
                    // Activate the ability here
                    //StartCoroutine(cardInZone.ActivateAbility(TurnBasedController.Instance.IsPlayerTurn())); //ActivateCardInZone(cardInZone));
                }
            }
        }
        [SerializeField] private bool occupied;
        public bool Occupied
        {
            get { return occupied; }
            set
            {
                occupied = value;
                //image.sprite = (occupied) ? cardInZone.cardInfo.image : null;

                if (!GetComponent<UIGrave>() || !GetComponent<UIDeck>())
                {
                    //image.color = new Color(1, 1, 1, (occupied) ? 1 : 0);
                }
            }
        }
        public int zoneIndex;

        [Header("Visuals")]
        //public Image image;
        public Animator zoneFlash;
        public GameObject attackIcon;

        private bool hasAttacked = false;
        public bool HasAttacked
        {
            get { return hasAttacked; }
            set
            {
                hasAttacked = value;
            }
        }

        [SerializeField] private string instanceId;
        public string InstanceId
        {
            get { return instanceId; }
            set { instanceId = value; }
        }

        #endregion

        #region Initialisation

        private void Start()
        {
            if (zoneType != ZoneType.Grave)
            {
                //image = GetComponent<Image>();
            }
        }

        #endregion

        #region Updating

        private void Update()
        {
            //if (attackIcon != null && Occupied)
            //{
            //    if (CombatManager.Instance.CurrentPhase == Phase.BattlePhase)
            //    {
            //        if (CombatManager.Instance.currentTurn == Turn.Player)
            //        {
            //            attackIcon.SetActive(true);
            //        }
            //    }
            //    else
            //    {
            //        attackIcon.SetActive(false);
            //    }
            //}
        }

        #endregion

        #region Clicking

        public void OnPointerClick(PointerEventData _data)
        {
            if (cardInZone && isPlayerZone)
            {
                //TurnBasedController.Instance.ShowInfoWindow(0, cardInZone);

                return;
            }
            else if (cardInZone && !isPlayerZone)
            {
                //TurnBasedController.Instance.ShowInfoWindow(0, cardInZone);

                return;
            }

            bool isAttacking = TurnBasedController.Instance.IsAttacking();

            if (isAttacking && !isPlayerZone && Occupied)
            {
                TurnBasedController.Instance.SetAttackTarget(this);
                return;
            }
        }

        #endregion
    }
}