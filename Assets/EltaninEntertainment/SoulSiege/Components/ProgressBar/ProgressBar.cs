﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace EltaninEntertainment.SoulSiege.Components.ProgressBars
{
    public class ProgressBar : MonoBehaviour
    {
        #region Variables

        [Header("Slider")]
        [SerializeField] private Slider slider;
        [SerializeField] private Image fill;

        [Header("Gradient")]
        [SerializeField] private Gradient gradient;

        [Header("Internal Values")]
        [SerializeField] private int fillValue;
        public int FillValue
        {
            get { return fillValue; }
            set
            {
                StartCoroutine(ValueChanged(value));
            }
        }
        
        [Header("Text")]
        [SerializeField] private TextMeshProUGUI text;
        
        [Header("Debug")]
        public int mod = 10;

        #endregion

        #region Events

        public delegate void PlayerLifeChanged(int _life);
        public PlayerLifeChanged OnPlayerLifeChanged;

        #endregion

        #region Initialisation

        private void OnEnable()
        {
            slider.onValueChanged.AddListener(SliderValueChanged);
            text.text = FillValue.ToString();
        }
        private void OnDisable()
        {
            slider.onValueChanged.RemoveListener(SliderValueChanged);
        }

        #endregion

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                FillValue += mod;
            }
        }

        #region Transition

        private IEnumerator ValueChanged(int _value)
        {
            bool addition = _value > slider.value;

            while (_value != slider.value)
            {
                if (addition)
                {
                    slider.value += 1;
                    text.text = slider.value.ToString();
                }
                else
                {
                    slider.value -= 1;
                    text.text = slider.value.ToString();
                }
                yield return new WaitForEndOfFrame();
            }
            fillValue = _value;
        }

        private void SliderValueChanged(float _value)
        {
            float normalisedValue = _value / slider.maxValue;
            fill.color = gradient.Evaluate(normalisedValue);
        }

        #endregion

        private void OnValidate()
        {
            if (!slider) { slider = GetComponent<Slider>(); }
            if (!text) { text = GetComponentInChildren<TextMeshProUGUI>(); }

            fill.color = gradient.Evaluate(slider.value);
            text.text = slider.value.ToString();
        }
    }
}