﻿using EltaninEntertainment.SoulSiege.Enums.Abilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.Components.Abilities
{
    [System.Serializable]
    public class Ability
    {
        [Header("Manadory Values")]
        [TextArea(0, 5)] public string text;
        public AbilityClassification category;
        public AbilityType type;

        [Header("Cost Parameters")]
        public AbilityCost cost;
        public int lifeCost;
        public int soulEnergyCost;
        public int discardNumber;
        public int offeringNumber;
        public int shuffleHandNumber;
        public int shuffleFieldNumber;

        [Header("Draw Parameters")]
        public int numberToDraw;
        public int numberToSummon;
        
        [Header("Search Parameters")]
        public SearchType searchType;
        public List<string> searchParameters;

        [Header("Soul Energy Parameters")]
        public int soulEnergyModifier;

        [Header("Strength Parameters")]
        public int strModifier;
        
        [Header("Stamina Parameters")]
        public int staModifier;

        [Header("Summon Limit Parameters")]
        public int summonLimitModifier;

        [Header("Life Parameters")]
        public int lifeModifier;
    }
}