﻿using EltaninEntertainment.Commons.Battle;
using EltaninEntertainment.SoulSiege.Enums.Cards;
using EltaninEntertainment.SoulSiege.Enums.Zones;
using EltaninEntertainment.SoulSiege.Components.Cards;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.Components
{
    public class CardZone : MonoBehaviour
    {
        #region Variables

        [Header("Zone Type")]
        public ZoneType zoneType;

        [Header("Zone Info")]
        [SerializeField] private Card cardInZone;
        public Card CardInZone
        {
            get { return cardInZone; }
            set
            {
                cardInZone = value;
                Occupied = cardInZone != null;

                if (cardInZone != null && cardInZone.cardType == CardType.Tactic)
                {
                    //StartCoroutine(cardInZone.ActivateAbility(TurnBasedController.Instance.IsPlayerTurn())); //ActivateCardInZone(cardInZone));
                }
            }
        }
        [SerializeField] private bool occupied;
        public bool Occupied
        {
            get { return occupied; }
            set
            {
                occupied = value;
                spriteRenderer.sprite = (occupied) ? cardInZone.image : null;
            }
        }
        public int zoneIndex;

        [Header("Visuals")]
        public SpriteRenderer spriteRenderer;
        public GameObject attackIcon;

        #endregion

        // Use this for initialization
        void Start()
        {
            if (zoneType != ZoneType.Grave)
            {
                spriteRenderer = GetComponent<SpriteRenderer>();
            }
        }

        /* void Update()
        {
            // This needs to be moved out of an update function
            if (attackIcon != null && Occupied)
            {
                if (TurnBasedController.Instance.CurrentPhase == Phase.BattlePhase)
                {
                    if (TurnBasedController.Instance.IsPlayerTurn())
                    {
                        attackIcon.SetActive(true);
                    }
                }
                else
                {
                    attackIcon.SetActive(false);
                }
            }
        } */

        private void OnMouseDown()
        {
            if (cardInZone)
            {
                //TurnBasedController.Instance.ShowInfoWindow(0, cardInZone, zoneIndex);
            }
        }

        private void OnValidate()
        {
            //if (!attackIcon && transform.GetComponentInChildren<GameObject>() ) { attackIcon = transform.GetChild(0).gameObject; }
            if (!spriteRenderer) { spriteRenderer = GetComponent<SpriteRenderer>(); }
        }
    }
}