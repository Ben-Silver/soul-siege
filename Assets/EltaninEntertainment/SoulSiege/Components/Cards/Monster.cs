﻿using EltaninEntertainment.SoulSiege.Enums.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.Components.Cards
{
    /// <summary>
    /// <para>Monsters are the main way to win.</para>
    /// <para>They have strength and stamina, also race and abilities.</para>
    /// </summary>
    [CreateAssetMenu(fileName = "Monster", menuName = "Card/Monster", order = 0)]
    public class Monster : Card
    {
        [Header("Monster Type")]
        public Race race;

        [Header("Stats")]
        [Range(1,10)] public int soulEnergy = 1;
        public int Strength;
        public int Stamina;
    }
}