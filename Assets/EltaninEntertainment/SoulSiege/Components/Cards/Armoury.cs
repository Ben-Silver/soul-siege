﻿using EltaninEntertainment.SoulSiege.Components.Abilities;
using EltaninEntertainment.SoulSiege.Enums.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.Components.Cards
{
    /// <summary>
    /// <para>Armoury gives monsters extra boosts to offence, defence, etc.</para>
    /// </summary>
    [CreateAssetMenu(fileName = "Armoury", menuName = "Card/Armoury", order = 3)]
    public class Armoury : Card
    {
        public Race equipProperty;
    }
}