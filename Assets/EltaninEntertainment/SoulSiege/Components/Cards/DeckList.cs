﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.Components.Cards
{
    [CreateAssetMenu(fileName = "Deck List", menuName = "Deck/Deck List", order = 0)]
    public class DeckList : ScriptableObject
    {
        [Header("The current deck list")]
        public Deck activeDeck;
        public string activeDeckName;

        [Header("All the player's deck lists")]
        public List<Deck> deckLists;
        public List<string> deckListNames;
    }
}