﻿using EltaninEntertainment.SoulSiege.Enums.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.Components.Cards
{
    /// <summary>
    /// <para>Terrain cards can boosts monsters with certain properties.</para>
    /// <para>Terrain cards can also weaken monsters with certain properties.</para>
    /// </summary>
    [CreateAssetMenu(fileName = "Terrain", menuName = "Card/Terrain", order = 2)]
    public class Terrain : Card
    {
        public Race boostProperty;
        public Race weakenProperty;
        public int boostValue;
        public int weakenValue;
    }
}