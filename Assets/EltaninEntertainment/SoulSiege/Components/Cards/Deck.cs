﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.Components.Cards
{
    [CreateAssetMenu(fileName = "Deck", menuName = "Deck/Deck", order = 1)]
    public class Deck : ScriptableObject
    {
        public string deckName;
        public List<Card> deck;
        //public List<string> deck;
    }
}