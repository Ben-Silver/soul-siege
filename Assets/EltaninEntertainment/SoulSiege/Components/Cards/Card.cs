﻿using EltaninEntertainment.SoulSiege.Enums.Abilities;
using EltaninEntertainment.SoulSiege.Enums.Cards;
using EltaninEntertainment.SoulSiege.Enums.Zones;
using EltaninEntertainment.SoulSiege.Components.Abilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EltaninEntertainment.Commons.Battle;
using EltaninEntertainment.SoulSiege.Components.Interactables;

namespace EltaninEntertainment.SoulSiege.Components.Cards
{
    public class Card : ScriptableObject
    {
        #region Variables

        [Header("Details")]
        public string cardName;
        public string IDNumber;

        [Header("Type")]
        public CardType cardType;
        public CardSubtype cardSubtype;
        public Rarity rarity;
        
        [Header("Images")]
        public Sprite image;
        public Sprite imageRaw;

        [Header("Abilities")]
        public Ability ability;

        [Header("UI Card Prefab")]
        public UICard cardPrefab;

        #endregion

        #region Initialisation

        #endregion
    }
}