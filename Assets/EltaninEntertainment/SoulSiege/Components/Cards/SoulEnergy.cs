﻿using EltaninEntertainment.SoulSiege.Components.Abilities;
using EltaninEntertainment.SoulSiege.Enums.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.Components.Cards
{
    /// <summary>
    /// <para>Soul Energy enables monster abilities</para>
    /// </summary>
    [CreateAssetMenu(fileName = "Soul Energy", menuName = "Card/Soul Energy", order = 4)]
    public class SoulEnergy : Card
    {
        public Race soulEnergyProperty;
    }
}