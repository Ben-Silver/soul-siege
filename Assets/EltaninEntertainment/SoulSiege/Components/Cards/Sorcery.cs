﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.Components.Cards
{
    /// <summary>
    /// <para>Sorcery cards mainly grant extra consistency, power, etc.</para>
    /// </summary>
    [CreateAssetMenu(fileName = "Sorcery", menuName = "Card/Sorcery", order = 1)]
    public class Sorcery : Card
    {

    }
}