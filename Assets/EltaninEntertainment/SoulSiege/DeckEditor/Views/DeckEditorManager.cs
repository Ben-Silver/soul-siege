﻿using EltaninEntertainment.SoulSiege.Components.Cards;
using EltaninEntertainment.SoulSiege.Resources;
using EltaninEntertainment.SoulSiege.Components.Interactables;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace EltaninEntertainment.SoulSiege.DeckEditor.Views
{
    public class DeckEditorManager : MonoBehaviour
    {
        [Header("GUI Components")]
        public UIEditorCard uiCardPrefab;
        public Transform availableCardWindow;
        public Transform deckWindow;
        public TMP_Dropdown dropdown;
        public TMP_InputField deckNameInput;

        [Header("Deck Components")]
        public Deck activeDeck;
        public DeckList deckList;
        public List<UIEditorCard> uiDeckList;

        [Header("Available Cards")]
        public List<Card> availableCards;
        public List<UIEditorCard> availableUICards;

        [Header("Dragging")]
        public UIEditorCard draggedCard;

        #region Initialisation

        // Start is called before the first frame update
        void Start()
        {
            availableCards = ResourceDatabase.Instance.cardsDatabase.cards;

            foreach (Card card in availableCards)
            {
                UIEditorCard newCard = Instantiate(uiCardPrefab, availableCardWindow);
                newCard.SetCard(card);
                availableUICards.Add(newCard);
            }

            LoadDecksFromJson();

            PopulateDropdown();
        }

        private void OnEnable()
        {
            // Subscribe to the onValueChanged event
            dropdown.onValueChanged.AddListener(ActiveDeckChanged);
        }

        private void OnDisable()
        {
            // Unsubscribe from the onValueChanged event
            dropdown.onValueChanged.RemoveListener(ActiveDeckChanged);
            deckList.deckLists.Clear();
        }

        /// <summary>
        /// Loads all the deck files from the file system
        /// </summary>
        private void LoadDecksFromJson()
        {
            foreach (string name in deckList.deckListNames)
            {
                // Create the path with the specified name
                string path = Application.dataPath + "/EltaninEntertainment/SoulSiege/Resources/DeckFiles/" + name + ".ssdf";

                // If the path exists
                if (File.Exists(path))
                {
                    // Create a new binary formatter
                    BinaryFormatter bf = new BinaryFormatter();

                    // Open the file at the specified path
                    FileStream file = File.Open(path, FileMode.Open);

                    // Create a new deck and deserialise the file into it
                    FormattedDeck newFormattedDeck = ScriptableObject.CreateInstance<FormattedDeck>();
                    JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), newFormattedDeck);
                    
                    Deck newDeck = ScriptableObject.CreateInstance<Deck>();

                    newDeck.deck = ResourceDatabase.Instance.GetDeckList(newFormattedDeck.deck);
                    newDeck.deckName = newFormattedDeck.deckName;

                    // Create a new deck and deserialise the file into it
                    //Deck newDeck = ScriptableObject.CreateInstance<Deck>();
                    //JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), newDeck);

                    // Close the file
                    file.Close();

                    newDeck.name = newDeck.deckName;
                    deckList.deckLists.Add(newDeck);
                    if (newDeck.deckName == deckList.activeDeckName)
                    {
                        deckList.activeDeck = newDeck;
                        activeDeck = newDeck;
                    }
                }
            }
        }

        #endregion

        #region Switching Deck

        /// <summary>
        /// When the deck editor is 
        /// </summary>
        private void PopulateDropdown()
        {
            // Create a new list of dropdown options
            List<TMP_Dropdown.OptionData> newData = new List<TMP_Dropdown.OptionData>();
            foreach (Deck deck in deckList.deckLists)
            {
                TMP_Dropdown.OptionData optionData = new TMP_Dropdown.OptionData(deck.deckName);
                newData.Add(optionData);
            }

            // Add the new data to the dropdown
            dropdown.AddOptions(newData);

            int deckIndex = deckList.deckLists.FindIndex(deck => deck.deckName == deckList.activeDeck.deckName);
            //Debug.Log(deckIndex);
            dropdown.value = deckIndex;
            ActiveDeckChanged(deckIndex);
        }

        /// <summary>
        /// Called when the dropdown option is changed
        /// </summary>
        /// <param name="_deckIndex"></param>
        public void ActiveDeckChanged(int _deckIndex)
        {
            activeDeck = deckList.deckLists[_deckIndex];
            deckList.activeDeck = activeDeck;
            deckList.activeDeckName = activeDeck.deckName;

            foreach (UIEditorCard card in uiDeckList)
            {
                Destroy(card.gameObject);
            }
            uiDeckList.Clear();

            foreach (Card card in activeDeck.deck)
            {
                UIEditorCard newCard = Instantiate(uiCardPrefab, deckWindow);
                newCard.SetCard(card);
                uiDeckList.Add(newCard);
            }
        }

        public void ExitDeckEditor()
        {
            deckList.activeDeck = activeDeck;
        }

        #endregion

        public void CreateDeck()
        {
            string newDeckName = deckNameInput.text;
            
            if (deckList.deckListNames.Contains(newDeckName)) { return; }

            Deck deck = ScriptableObject.CreateInstance<Deck>();
            deck.deckName = newDeckName;
            deck.name = newDeckName;
            activeDeck = deck;
            deckList.deckListNames.Add(newDeckName);
            deckList.deckLists.Add(deck);
        }

        public void SaveDeck()
        {
            FormattedDeck newFormattedDeck = ScriptableObject.CreateInstance<FormattedDeck>();
            newFormattedDeck.deck = new List<string>();

            foreach (UIEditorCard card in uiDeckList)
            {
                newFormattedDeck.deck.Add(card.cardInfo.cardName);
                Debug.Log("Added " + newFormattedDeck.deck[newFormattedDeck.deck.Count - 1]);
            }
            newFormattedDeck.deckName = activeDeck.deckName;

            string path = Application.dataPath + "/EltaninEntertainment/SoulSiege/Resources/DeckFiles/" + newFormattedDeck.deckName + ".ssdf";

            BinaryFormatter bf = new BinaryFormatter();
            if (File.Exists(path))
            {
                File.Delete(Application.dataPath + "/EltaninEntertainment/SoulSiege/Resources/DeckFiles/" + newFormattedDeck.deckName + ".ssdf");
            }
            FileStream file = File.Create(Application.dataPath + "/EltaninEntertainment/SoulSiege/Resources/DeckFiles/" + newFormattedDeck.deckName + ".ssdf");
            var json = JsonUtility.ToJson(newFormattedDeck);
            bf.Serialize(file, json);
            file.Close();
        }

        // Update is called once per frame
        void Update()
        {

        }

        /* private void OnValidate()
        {
            activeDeck = deckList.activeDeck;
        } */
    }
}