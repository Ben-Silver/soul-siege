﻿using EltaninEntertainment.SoulSiege.Components.Interactables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace EltaninEntertainment.SoulSiege.DeckEditor.Views
{
    public class DeckArea : DropZone
    {
        public DeckEditorManager deckEditor;

        public override void OnPointerEnter(PointerEventData _data)
        {
            base.OnPointerEnter(_data);

            deckEditor.uiDeckList.Remove(deckEditor.draggedCard);
        }

        public override void OnDrop(PointerEventData _data)
        {
            base.OnDrop(_data);

            deckEditor.uiDeckList.Add(deckEditor.draggedCard);
        }
    }
}