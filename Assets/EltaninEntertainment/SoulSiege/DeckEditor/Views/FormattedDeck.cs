﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.DeckEditor.Views
{
    public class FormattedDeck : ScriptableObject
    {
        public string deckName;
        public List<string> deck;
    }
}