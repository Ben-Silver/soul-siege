﻿using EltaninEntertainment.Commons.Battle;
using EltaninEntertainment.SoulSiege.Components.Cards;
using EltaninEntertainment.SoulSiege.Components.Interactables;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace EltaninEntertainment.SoulSiege.DeckEditor.Views
{
    public class UIEditorCard : Draggable
    {
        #region Variables

        [Header("Card Settings")]
        public Card cardInfo;
        public Image cardImage;
        public Sprite cardBack;

        [SerializeField] private string instanceId;
        /// <summary>
        /// <br>Remains unique to the gameObject.</br>
        /// <br>Necessary because Unity cannot differentiate between 2+ of the same ScriptableObjects.</br>
        /// </summary>
        public string InstanceId
        {
            get { return instanceId; }
            set { instanceId = value; }
        }

        #endregion

        #region Initialisation

        protected override void Start()
        {
            base.Start();

            initialPosition = rectTransform.position;
            if (cardInfo) { SetCard(cardInfo); }

            InstanceId = StringGenerator.GenerateID(8);
        }

        public void SetCard(Card _cardInfo)
        {
            cardInfo = _cardInfo;
            gameObject.name = cardInfo.cardName;
            cardImage.sprite = _cardInfo.image;
        }

        #endregion

        #region Clicking

        public override void OnDrag(PointerEventData _data)
        {
            base.OnDrag(_data);

            FindObjectOfType<DeckEditorManager>().draggedCard = this;
        }

        public override void OnEndDrag(PointerEventData _data)
        {
            base.OnEndDrag(_data);

            FindObjectOfType<DeckEditorManager>().draggedCard = null;
        }

        public override void OnPointerClick(PointerEventData _data)
        {
            base.OnPointerClick(_data);

            if (!dragging && IsInteractable && !TurnBasedController.Instance.viewingDeck)
            {
                // Get the card info
                Debug.Log("Clicking on " + cardInfo.cardName);
            }
        }

        #endregion
    }
}