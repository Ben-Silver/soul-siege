﻿using EltaninEntertainment.SoulSiege.Components.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.Resources
{
    public class ResourceDatabase : Singleton<ResourceDatabase>
    {
        public Cards cardsDatabase;

        public List<Card> GetAllCards()
        {
            return cardsDatabase.cards;
        }

        public Card QueryCardDatabase(string _cardName)
        {
            foreach (Card card in cardsDatabase.cards)
            {
                if (_cardName == card.cardName)
                {
                    //Debug.Log(card);
                    return card;
                }
            }
            return null;
        }

        public List<Card> GetDeckList(List<string> _deckList)
        {
            List<Card> deckList = new List<Card>();
            foreach (string card in _deckList)
            {
                Card newCard = QueryCardDatabase(card);

                if (newCard)
                {
                    deckList.Add(newCard);
                }
            }
            return cardsDatabase.cards;
        }
    }
}