﻿using EltaninEntertainment.SoulSiege.Components.Cards;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.SoulSiege.Resources
{
    [CreateAssetMenu(fileName = "Card Database", menuName = "Asset Database/Card Database")]
    public class Cards : ScriptableObject
    {
        public List<Card> cards;
    }
}