﻿namespace EltaninEntertainment.SoulSiege.Enums.Zones
{
    public enum ZoneType : int
    {
        Unit,
        Tactic,
        Terrain,
        Deck,
        Grave
    }
}