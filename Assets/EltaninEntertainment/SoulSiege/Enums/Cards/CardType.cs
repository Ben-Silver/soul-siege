﻿namespace EltaninEntertainment.SoulSiege.Enums.Cards
{
    /// <summary>
    /// The card type
    /// </summary>
    public enum CardType : int
    {
        Unit,
        Tactic,
        Equip
    }

    public enum CardSubtype : int
    {
        Normal,
        Legendary
    }
}