﻿namespace EltaninEntertainment.SoulSiege.Enums.Cards
{
    /// <summary>
    /// The race of monster
    /// </summary>
    public enum Race : int
    {
        None,
        Amphibian,
        Arthropod,
        Avian,
        Cyborg,
        Demon,
        Dinosaur,
        Dragon,
        Elf,
        Fish,
        Human,
        Lightning,
        Machine,
        Mammal,
        Phantom,
        Plant,
        Pyro,
        Reptile,
        Rock,
        Undead,
    }
}