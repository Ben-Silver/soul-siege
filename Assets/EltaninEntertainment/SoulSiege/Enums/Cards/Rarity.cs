﻿namespace EltaninEntertainment.SoulSiege.Enums.Cards
{
    /// <summary>
    /// The possible card rarities
    /// </summary>
    public enum Rarity : int
    {
        Normal,
        Rare,
        SuperRare,
        UltraRare
    }
}