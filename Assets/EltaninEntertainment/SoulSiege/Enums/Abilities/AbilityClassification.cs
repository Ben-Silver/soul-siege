﻿namespace EltaninEntertainment.SoulSiege.Enums.Abilities
{
    public enum AbilityClassification : int
    {
        /// <summary>
        /// Activates when you choose to
        /// </summary>
        Ignition,
        /// <summary>
        /// Activates when an action happens
        /// </summary>
        Trigger,
        /// <summary>
        /// Doesn't Activate, is constantly active
        /// </summary>
        Continuous
    }
}