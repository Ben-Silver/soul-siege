﻿namespace EltaninEntertainment.SoulSiege.Enums.Abilities
{
    /// <summary>
    /// The possible effect types
    /// </summary>
    public enum AbilityType : int
    {
        None,
        Draw,
        Search,
        ModifySoulEnergy,
        ModiftyStrength,
        ModifyStamina,
        DestroyMonster,
        ModifySummonLimit,
        SpecialSummonHand,
        SpecialSummonGrave,
        SpecialSummonDeck,
        RecoverLife,
        DamageLife,
        RecoverOpponentLife,
        DamageOpponentLife,
    }
}