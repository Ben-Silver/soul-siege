﻿namespace EltaninEntertainment.SoulSiege.Enums.Abilities
{
    public enum AbilityCost : int
    {
        /// <summary>
        /// Lucky you!
        /// </summary>
        None,
        /// <summary>
        /// Pay life
        /// </summary>
        Life,
        /// <summary>
        /// Soul Energy
        /// </summary>
        SoulEnergy,
        /// <summary>
        /// Send from hand to discard pile
        /// </summary>
        Discard,
        /// <summary>
        /// Send from field to discard pile
        /// </summary>
        Offering,
        /// <summary>
        /// Shuffle from hand into deck
        /// </summary>
        ShuffleFromHand,
        /// <summary>
        /// Shuffle from field into deck
        /// </summary>
        ShuffleFromField,
    }
}