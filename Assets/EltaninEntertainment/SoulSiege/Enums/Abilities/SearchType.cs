﻿namespace EltaninEntertainment.SoulSiege.Enums.Abilities
{
    public enum SearchType
    {
        Name,
        Type,
        ATK,
        DEF,
        Level
    }
}