﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ListExtensions<T>
{
    public static T item;

    public delegate void ListChanged(T _item);
    public static ListChanged OnListChanged;

    public static void Add(List<T> _list, T _value)
    {
        _list.Add(_value);
        OnListChanged?.Invoke(_value);
    }
    
    public static void Remove(List<T> _list, T _value)
    {
        _list.Remove(_value);
        OnListChanged?.Invoke(_value);
    }

    public static void RemoveAt(List<T> _list, int _index)
    {
        _list.RemoveAt(_index);
        OnListChanged?.Invoke(_list[_index]);
    }
    public static void RemoveAt(List<T> _list, T _value, int _index)
    {
        _list.RemoveAt(_index);
        OnListChanged?.Invoke(_value);
    }
}
