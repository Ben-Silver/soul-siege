using EltaninEntertainment.Commons.Battle;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.Commons.Cards
{
    public class CardStepTributeCard : CardStep
    {
        public override IEnumerator Execute()
        {
            TurnBasedController.Instance.MoveCardToGrave(0);
            yield return new WaitForSeconds(1.5f);
        }
    }
}