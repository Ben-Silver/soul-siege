using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.Commons.Cards
{
	public class Card : MonoBehaviour
	{
		public List<CardStep> cardSteps;

		public IEnumerator ExcecuteCardEffect()
		{
			Debug.Log("Activating effect");

			foreach (CardStep cardStep in cardSteps)
				yield return cardStep.Execute();

			yield return null;
		}
	}
}