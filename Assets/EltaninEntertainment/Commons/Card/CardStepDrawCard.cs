using EltaninEntertainment.Commons.Battle;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.Commons.Cards
{
    public class CardStepDrawCard : CardStep
    {
        public int cardsToDraw;

        public override IEnumerator Execute()
        {
            bool isPlayer = TurnBasedController.Instance.IsPlayerTurn();

            for (int i = 0; i < cardsToDraw; i++)
            {
                TurnBasedController.Instance.Draw(isPlayer);
                yield return new WaitForSeconds(1.5f);
            }

            yield return null;
        }
    }
}