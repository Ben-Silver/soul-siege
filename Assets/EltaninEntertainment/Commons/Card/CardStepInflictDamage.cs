using EltaninEntertainment.Commons.Battle;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.Commons.Cards
{
    public class CardStepInflictDamage : CardStep
    {
        public int damage;

        public override IEnumerator Execute()
        {
            bool isPlayer = TurnBasedController.Instance.IsPlayerTurn();

            if (isPlayer)
                TurnBasedController.Instance.GetPlayer().life -= damage;
            else
                TurnBasedController.Instance.GetOpponent().life -= damage;

            yield return null;
        }
    }
}