using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.Commons.Cards
{
	/// <summary>
	/// Create gameObject with children of this class
	/// </summary>
	public abstract class CardStep : MonoBehaviour
	{
		[SerializeField] protected Card card;

		public abstract IEnumerator Execute();

		#region Editor
#if UNITY_EDITOR
		private void OnValidate()
		{
			if (!card)
				card = GetComponentInParent<Card>();
		}
#endif
		#endregion
	}
}