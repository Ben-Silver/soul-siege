﻿using EltaninEntertainment.SoulSiege.Components;
using EltaninEntertainment.SoulSiege.Components.Cards;
using EltaninEntertainment.SoulSiege.Components.Interactables;
using EltaninEntertainment.SoulSiege.Components.Views;
using EltaninEntertainment.SoulSiege.Enums.Cards;
using EltaninEntertainment.SoulSiege.Enums.Zones;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.Commons.Battle
{
    public class BoardManager : MonoBehaviour
    {
        #region Variables

        [Header("Board Components")]
        public UIDeck mainDeck;
        public UIHand hand;
        public UIGrave grave;
        public UIDeck mainDeckOpp;
        public UIHand handOpp;
        public UIGrave graveOpp;

        #endregion

        #region Hand Managers

        /// <summary>
        /// Function to draw a card from a player's deck
        /// </summary>
        public void DrawCard(bool _isPlayer)
        {
            if (_isPlayer)
            {
                DiscardAndDraw(hand.handSlots.Count - 1, true);

                // Add the top card of the deck to the hand
                hand.OnCardAdded?.Invoke(mainDeck.deck[mainDeck.deck.Count - 1]);
                mainDeck.deck.RemoveAt(mainDeck.deck.Count - 1);
                mainDeck.OnDeckSizeChanged?.Invoke();
            }
            else
            {
                DiscardAndDraw(handOpp.handSlots.Count - 1, false);

                handOpp.OnCardAdded?.Invoke(mainDeckOpp.deck[mainDeckOpp.deck.Count - 1]);
                mainDeckOpp.deck.RemoveAt(mainDeckOpp.deck.Count - 1);
                mainDeckOpp.OnDeckSizeChanged?.Invoke();
            }
        }

        /// <summary>
        /// Function to discard a card from a player's hand
        /// </summary>
        public void DiscardAndDraw(int _indexNumber, bool _isPlayer)
        {
            if (_isPlayer)
            {
                if (hand.handSlots.Count > TurnBasedController.Instance.handLimit)
                {
                    Debug.Log("Player has too many cards");
                    for (int i = TurnBasedController.Instance.handLimit; i < hand.handSlots.Count; i++)
                    {
                        Debug.Log("Discarding from player's hand");
                        StartCoroutine(MoveCardToZone(hand.handSlots[_indexNumber],
                            TurnBasedController.Instance.GetZone(12, true)));
                    }
                }
            }
            else
            {
                if (handOpp.handSlots.Count > TurnBasedController.Instance.handLimit)
                {
                    Debug.Log("Opponent has too many cards");
                    for (int i = TurnBasedController.Instance.handLimit; i < handOpp.handSlots.Count; i++)
                    {
                        Debug.Log("Discarding from opponent's hand");
                        StartCoroutine(MoveCardToZone(handOpp.handSlots[_indexNumber],
                            TurnBasedController.Instance.GetZone(12, false)));
                    }
                }
            }
        }

        #endregion

        public void MoveCardToGrave(UICard _card)
        {
            UICardZone zone = GetValidZones(ZoneType.Grave, true).Find((_zone) =>
            {
                return _zone.zoneType == ZoneType.Grave;
            });

            StartCoroutine(MoveCardToZone(_card, zone, () =>
            { 
                grave.grave.Add(_card.cardInfo);
            }));
        }

        #region Placing Cards

        public void PlaceUICard()
        {
            UICard selectedCard = TurnBasedController.Instance
                                                     .GetInfoWindow(0)
                                                     .selectedCard;

            bool isPlayerTurn = TurnBasedController.Instance.IsPlayerTurn();

            if (selectedCard.cardInfo.cardType == CardType.Unit)
            {
                PlaceUnit
                (
                    selectedCard,
                    isPlayerTurn
                );
            }
            else if (selectedCard.cardInfo.cardType == CardType.Tactic)
            {
                PlaceTactic
                (
                    selectedCard,
                    isPlayerTurn
                );
            }
        }
        
        public void PlaceUnit(UICard _card, bool _isPlayer)
        {
            List<UICardZone> validCardZones = GetValidZones(ZoneType.Unit, _isPlayer);

            if (TurnBasedController.Instance.numberOfSummons > 0)
            {
                // Get the exact card we want and move it to the zone we want
                UICard cardToPlace = _card;

                StartCoroutine(MoveCardToZone(cardToPlace, validCardZones[0]));
            }
        }

        public void PlaceUnit(UICard _card, UICardZone _zone)
        {
            if (TurnBasedController.Instance.numberOfSummons > 0)
            {
                // Get the exact card we want and move it to the zone we want
                UICard cardToPlace = _card;

                StartCoroutine(MoveCardToZone(cardToPlace, _zone));
            }
        }

        public void PlaceTactic(UICard _card, bool _isPlayer)
        {
            List<UICardZone> validCardZones = GetValidZones(ZoneType.Tactic, _isPlayer);

            // Get the exact card we want and move it to the zone we want
            UICard cardToPlace = _card;

            StartCoroutine(MoveCardToZone(cardToPlace, validCardZones[0]));
        }

        public void PlaceTactic(UICard _card, UICardZone _zone)
        {
            // Get the exact card we want and move it to the zone we want
            UICard cardToPlace = _card;

            StartCoroutine(MoveCardToZone(cardToPlace, _zone));
        }

        #endregion

        #region Zone Validation

        /// <summary>
        /// Returns a list of all the valid zones of the specified zone type
        /// </summary>
        public List<UICardZone> GetValidZones(ZoneType _zoneType, bool _isPlayer)
        {
            // Get a list of all zones
            List<UICardZone> allZones = TurnBasedController.Instance.GetAllZones(_isPlayer);
            List<UICardZone> validZones = new List<UICardZone>();

            // Loop through all the zones, depending on whether we want the player or opponent zones
            foreach (UICardZone zone in allZones)
            {
                // If the zone is the correct type and is free, add the zone to the list
                if (zone.zoneType == _zoneType && !zone.Occupied)
                {
                    //Debug.Log(zone.gameObject.name + ", " + zone.zoneIndex + ", " + zone.zoneType + ", " + zone.Occupied);
                    validZones.Add(zone);
                }
            }

            return validZones;
        }

        #endregion

        #region Zone "Animation"

        public void MoveCard(Card _card, string _instanceId, ZoneType _newZone)
        {
            UICardZone selectedCard = TurnBasedController.Instance
                                                         .FindCardInZones(_instanceId);
            if (selectedCard.CardInZone == _card)
            {
                bool playerTurn = TurnBasedController.Instance.IsPlayerTurn();

                List<UICardZone> validZones = GetValidZones(_newZone, playerTurn);
                StartCoroutine(MoveCardToZone(selectedCard, validZones[0]));
            }
        }

        /// <summary>
        /// <br>Sends a card with the passed _instanceId to a _newZone.</br>
        /// <br>The zone it goes to is dependent on the owner.</br>
        /// </summary>
        public void MoveCard(string _instanceId, ZoneType _newZone, bool _isPlayer)
        {
            UICardZone selectedCard = TurnBasedController.Instance.FindCardInZones(_instanceId, _isPlayer);

            StartCoroutine(MoveCardToZone(selectedCard, GetValidZones(_newZone, _isPlayer)[0]));
        }

        /// <summary>
        /// Moves a _card from the hand to a _zone
        /// </summary>
        public IEnumerator MoveCardToZone(UICard _card, UICardZone _zone, Action _onComplete = null)
        {
            InfoWindow window = TurnBasedController.Instance.GetInfoWindow(0);

            if (window.gameObject.activeSelf)
            {
                window.BackToActionMenu();
            }

            TurnBasedController.Instance.viewingDeck = true;
            bool isPlayer = TurnBasedController.Instance.IsPlayerTurn();

            // Smoothing
            Vector3 vel = Vector3.zero;
            Vector2 vel2 = Vector2.zero;

            // Rect manipulation
            RectTransform cardRect = _card.GetComponent<RectTransform>();
            RectTransform zoneRect = _zone.GetComponent<RectTransform>();
            
            // Parent the _card to the _zone it is being moved to
            _card.transform.SetParent(_zone.transform);
            
            while (Vector3.Distance(_card.transform.position, _zone.transform.position) > 0.25f)
            {
                _card.transform.position = Vector3.SmoothDamp(_card.transform.position, _zone.transform.position, ref vel, 0.1f);
                cardRect.sizeDelta = Vector2.SmoothDamp(cardRect.rect.size, zoneRect.rect.size, ref vel2, 0.1f);

                yield return new WaitForEndOfFrame();
            }

            if (_zone.zoneType == ZoneType.Grave)
            {
                _zone.GetComponent<UIGrave>().grave.Add(_card.cardInfo);
            }
            else
            {
                _zone.CardInZone = _card;
                _zone.InstanceId = _card.InstanceId;
            }

            if (_card.cardInfo.cardType == CardType.Unit) { TurnBasedController.Instance.numberOfSummons--; }

            if (isPlayer)
            {
                hand.OnCardRemoved?.Invoke(_card);
            }
            else
            {
                handOpp.OnCardRemoved?.Invoke(_card);
            }

            yield return new WaitForEndOfFrame();
            TurnBasedController.Instance.viewingDeck = false;

            _onComplete?.Invoke();
        }

        /// <summary>
        /// Moves a card from a _zone to a _newZone
        /// </summary>
        public IEnumerator MoveCardToZone(UICardZone _startZone, UICardZone _endZone)
        {
            // Smoothing
            Vector3 vel = Vector3.zero;
            Vector3 startPosition = _startZone.transform.position;

            while (Vector3.Distance(_startZone.transform.position, _endZone.transform.position) > 0.25f)
            {
                _startZone.transform.position = Vector3.SmoothDamp(_startZone.transform.position, _endZone.transform.position, ref vel, 0.2f);
                yield return new WaitForEndOfFrame();
            }

            if (_endZone.zoneType == ZoneType.Grave)
            {
                if (_endZone.isPlayerZone)
                {
                    // Put the card in the grave
                    grave.grave.Add(_startZone.CardInZone.cardInfo);
                    _startZone.CardInZone = null;
                }
                else
                {
                    // Put the card in the grave
                    graveOpp.grave.Add(_startZone.CardInZone.cardInfo);
                    _startZone.CardInZone = null;
                }
            }
            
            // Reset the position of the actual zone
            _startZone.transform.position = startPosition;

            yield return new WaitForEndOfFrame();
            TurnBasedController.Instance.viewingDeck = false;
        }
        
        #endregion
    }
}