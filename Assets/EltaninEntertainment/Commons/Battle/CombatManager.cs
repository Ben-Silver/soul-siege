﻿using EltaninEntertainment.Commons.Battle;
using EltaninEntertainment.SoulSiege.Components.Cards;
using EltaninEntertainment.SoulSiege.Components.Interactables;
using EltaninEntertainment.SoulSiege.Enums.Abilities;
using EltaninEntertainment.SoulSiege.Enums.Zones;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.Commons.Battle
{
    public class CombatManager : MonoBehaviour
    {
        #region Variables

        public List<UICardZone> availableZones;
        public List<UICardZone> attackableZones;
        public UICard attackingMonster;
        public Transform directAttackZone;
        public Transform directAttackZoneOpp;
        public UICardZone attackTarget;
        public bool selectingAttackTarget;

        #endregion

        #region Events

        public delegate void MonsterDestroyed(Monster _card);
        public MonsterDestroyed OnMonsterDestroyed;

        #endregion

        #region Zone Management

        /// <summary>
        /// Returns a list of all the valid zones of the specified zone type
        /// </summary>
        public List<UICardZone> GetAttackableZones(ZoneType _zoneType, bool _isPlayer)
        {
            // Get a list of all zones
            List<UICardZone> allZones = TurnBasedController.Instance.GetAllZones(_isPlayer);
            List<UICardZone> validZones = new List<UICardZone>();

            // Loop through all the zones, depending on whether we want the player or opponent zones
            foreach (UICardZone zone in allZones)
            {
                // If the zone is the correct type and is occupied, add the zone to the list
                if (zone.zoneType == _zoneType && zone.Occupied)
                {
                    //Debug.Log(zone.gameObject.name + ", " + zone.zoneIndex + ", " + zone.zoneType + ", " + zone.Occupied);
                    validZones.Add(zone);
                }
            }

            return validZones;
        }

        /// <summary>
        /// Populates the availableZones and attackableZones
        /// </summary>
        public void GetAvailableMonsterZones(bool _isPlayer)
        {
            availableZones = GetAttackableZones(ZoneType.Unit, _isPlayer);
            attackableZones = GetAttackableZones(ZoneType.Unit, !_isPlayer);

            foreach (UICardZone zone in availableZones)
            {
                zone.HasAttacked = false;
                if (zone.isPlayerZone) { zone.attackIcon.SetActive(true); }
            }

            if (!_isPlayer)
            {
                StartCoroutine(OpponentBattlePhase());
            }
        }

        /// <summary>
        /// Called when the battle phase ends
        /// </summary>
        public void ClearAvailableZones()
        {
            if (availableZones.Count > 0)
            {
                foreach (UICardZone zone in availableZones)
                {
                    if (zone.HasAttacked) { zone.HasAttacked = false; }
                    if (zone.isPlayerZone) { zone.attackIcon.SetActive(false); }
                }
            }

            availableZones.Clear();
            attackableZones.Clear();
        }

        #endregion

        #region Attacking

        /// <summary>
        /// Called when the attack button is pressed from within the CardInfoWindow
        /// </summary>
        public void DeclareAttack()
        {
            string instanceId = TurnBasedController.Instance
                                                   .GetInfoWindow(0)
                                                   .InstanceId;

            // Get the zone that is attacking, using the InstanceId
            UICardZone attackingZone = availableZones.Find((id) =>
            {
                return id.InstanceId == instanceId;
            });
            
            // Get the card in the zone and store it locally
            Monster attackingCard = (Monster)attackingZone.CardInZone.cardInfo;

            // Hide the info window
            TurnBasedController.Instance
                               .GetInfoWindow(0)
                               .BackToActionMenu();

            // Make it so the monster cannot attack again
            attackingZone.HasAttacked = true;
            attackingZone.attackIcon.SetActive(false);

            // Determine whether the attack will be on a monster or directly
            if (attackableZones.Count > 0)
            {
                StartCoroutine(SelectAttackTarget(attackingZone)); //MoveToAttackMonster(attackingZone, attackableZones[0]));
            }
            else
            {
                StartCoroutine(MoveToAttackDirectly(attackingZone, directAttackZone.position, attackingCard.Strength));
            }
        }

        /// <summary>
        /// Animates attacks and performs damage and destruction
        /// </summary>
        public IEnumerator MoveToAttackMonster(UICardZone _attackingZone, UICardZone _attackedZone)
        {
            // Game is busy
            TurnBasedController.Instance.viewingDeck = true;

            // Monster Conversion
            Monster attackingMonster = (Monster)_attackingZone.CardInZone.cardInfo;
            Monster attackedMonster = (Monster)_attackedZone.CardInZone.cardInfo;

            Debug.Log(attackingMonster);

            // Smoothing
            Vector3 vel = Vector3.zero;
            Vector3 startPosition = _attackingZone.transform.position;

            // Move the attacking monster to the attacked monster's position
            while (Vector3.Distance(_attackingZone.transform.position, _attackedZone.transform.position) > 0.25f)
            {
                _attackingZone.transform.position = Vector3.SmoothDamp(_attackingZone.transform.position, _attackedZone.transform.position, ref vel, 0.1f);
                yield return new WaitForEndOfFrame();
            }

            // Perform damage
            if (attackingMonster.Strength > attackedMonster.Strength)
            {
                // Subtract the difference from the attacked monster's owner's life
                if (_attackingZone.isPlayerZone)
                {
                    TurnBasedController.Instance.GetOpponent().life -= attackingMonster.Strength - attackedMonster.Strength;
                }
                // Subtract the difference from the attacking monster's owner's life
                else if (!_attackingZone.isPlayerZone)
                {
                    TurnBasedController.Instance.GetPlayer().life -= attackingMonster.Strength - attackedMonster.Strength;
                }
            }
            else if (attackingMonster.Strength < attackedMonster.Strength)
            {
                // Subtract the difference from the attacked monster's owner's life
                if (_attackingZone.isPlayerZone)
                {
                    TurnBasedController.Instance.GetPlayer().life -= attackedMonster.Strength - attackingMonster.Strength;
                }
                // Subtract the difference from the attacking monster's owner's life
                else if (!_attackingZone.isPlayerZone)
                {
                    TurnBasedController.Instance.GetOpponent().life -= attackedMonster.Strength - attackingMonster.Strength;
                }
            }

            // Reset the position of the actual zone
            while (Vector3.Distance(_attackingZone.transform.position, startPosition) > 0.25f)
            {
                _attackingZone.transform.position = Vector3.SmoothDamp(_attackingZone.transform.position, startPosition, ref vel, 0.1f);
                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForEndOfFrame();

            // Game is no longer busy
            TurnBasedController.Instance.viewingDeck = false;

            if (attackingMonster.Strength > attackedMonster.Strength)
            {
                // Destroy the attacked monster
                TurnBasedController.Instance.MoveCard(_attackedZone.InstanceId, ZoneType.Grave, _attackedZone.isPlayerZone);
                attackableZones.Remove(_attackedZone);

                Debug.Log("Monster wins");
            }
            else if (attackingMonster.Strength < attackedMonster.Strength)
            {
                // Destroy the attacking monster
                TurnBasedController.Instance.MoveCard(_attackingZone.InstanceId, ZoneType.Grave, _attackingZone.isPlayerZone);
                
                Debug.Log("Monster loses");
            }
            else if(attackingMonster.Strength == attackedMonster.Strength)
            {
                // Destroy both monsters
                TurnBasedController.Instance.MoveCard(_attackingZone.InstanceId, ZoneType.Grave, _attackingZone.isPlayerZone);
                TurnBasedController.Instance.MoveCard(_attackedZone.InstanceId, ZoneType.Grave, _attackedZone.isPlayerZone);
                attackableZones.Remove(_attackedZone);

                Debug.Log("Monster drew");
            }
        }

        /// <summary>
        /// Animates direct attacks and performs damage
        /// </summary>
        public IEnumerator MoveToAttackDirectly(UICardZone _zone, Vector3 _moveToPosition, int _damage)
        {
            // Game is busy
            TurnBasedController.Instance.viewingDeck = true;

            // Smoothing
            Vector3 vel = Vector3.zero;
            Vector3 startPosition = _zone.transform.position;

            // Move to the direct attack zone
            while (Vector3.Distance(_zone.transform.position, _moveToPosition) > 0.25f)
            {
                _zone.transform.position = Vector3.SmoothDamp(_zone.transform.position, _moveToPosition, ref vel, 0.1f);
                yield return new WaitForEndOfFrame();
            }

            // Subtract the strength from the attacked player's life
            if (_zone.isPlayerZone)
                TurnBasedController.Instance.GetPlayer().life -= _damage;
            else
                TurnBasedController.Instance.GetOpponent().life -= _damage;

            // Reset the position of the actual zone
            while (Vector3.Distance(_zone.transform.position, startPosition) > 0.25f)
            {
                _zone.transform.position = Vector3.SmoothDamp(_zone.transform.position, startPosition, ref vel, 0.1f);
                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForEndOfFrame();

            // Game is no longer busy
            TurnBasedController.Instance.viewingDeck = false;
        }

        #endregion

        /// <summary>
        /// Waits for the player to select an attack target
        /// </summary>
        public IEnumerator SelectAttackTarget(UICardZone _attackingZone)
        {
            foreach (UICardZone zone in attackableZones) { zone.zoneFlash.SetTrigger("StartFlashing"); }

            // Set the status as busy
            selectingAttackTarget = true;

            // Wait until the player selects a zone to attack
            while (!attackTarget) { yield return new WaitForEndOfFrame(); }
            
            // Set the status as no longer busy
            selectingAttackTarget = false;            

            // Attack the attack target
            StartCoroutine(MoveToAttackMonster(_attackingZone, attackTarget));
            foreach (UICardZone zone in attackableZones) { zone.zoneFlash.SetTrigger("StopFlashing"); }
            attackTarget = null;
        }

        #region Opponent Battling
        
        private IEnumerator OpponentBattlePhase()
        {
            List<UICardZone> zonesThatHaveAttacked = new List<UICardZone>();

            while (zonesThatHaveAttacked.Count != availableZones.Count)
            {
                foreach (UICardZone zone in availableZones)
                {
                    if (!zone.HasAttacked)
                    {
                        Debug.Log("We're doing this with " + zone.InstanceId);
                        StartCoroutine(OpponentDeclareAttack(zone));
                        zonesThatHaveAttacked.Add(zone);
                        yield return new WaitForSeconds(2);
                    }
                }
            }

            yield return new WaitForEndOfFrame();
        }

        private IEnumerator OpponentDeclareAttack(UICardZone _zone)
        {
            Monster cardP = (Monster)_zone.CardInZone.cardInfo;

            if (attackableZones.Count < 1)
            {
                StartCoroutine(MoveToAttackDirectly(_zone, directAttackZoneOpp.position, cardP.Strength));
                yield return new WaitForSeconds(2);
            }
            else
            {
                Debug.Log(_zone.InstanceId);

                // Do opponent stuff
                foreach (UICardZone opponentZone in attackableZones)
                {
                    Monster cardO = (Monster)opponentZone.CardInZone.cardInfo;

                    if (cardP.Strength >= cardO.Strength)
                    {
                        //attackingZone.HasAttacked = true;
                        StartCoroutine(MoveToAttackMonster(_zone, opponentZone));
                        yield return new WaitForSeconds(4);
                    }
                }
            }
            yield return null;
            StopCoroutine(OpponentDeclareAttack(_zone));
        }

        #endregion

        private void OnBattleEnd(Monster _card)
        {
            if (_card.ability.type == AbilityType.None) { }
        }
    }
}