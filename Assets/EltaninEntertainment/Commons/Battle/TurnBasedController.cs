using EltaninEntertainment.SoulSiege.Components;
using EltaninEntertainment.SoulSiege.Components.Cards;
using EltaninEntertainment.SoulSiege.Components.Interactables;
using EltaninEntertainment.SoulSiege.Components.Views;
using EltaninEntertainment.SoulSiege.Enums.Zones;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.Commons.Battle
{
    public class TurnBasedController : Singleton<TurnBasedController>
    {
        #region Turn Component Variables

        [Header("Turn Components")]
        public List<TurnStep> turnSteps;

        private int _turnIndex = -1;
        public int turnIndex
        {
            get { return _turnIndex; }
            set
            {
                if (_turnIndex != value)
                {
                    _turnIndex = value;
                    StartCoroutine(UpdateState(_turnIndex));
                }
            }
        }

        public BattleData data = null;

        #endregion

        #region UI Variables

        [Header("Board")]
        [SerializeField] private BoardManager boardManager;
        
        [Header("Battle")]
        [SerializeField] private CombatManager combatManager;

        [Header("Board Components")]
        public UIDeck mainDeck;
        public UIHand hand;
        public UIGrave grave;
        public UIDeck mainDeckOpp;
        public UIHand handOpp;
        public UIGrave graveOpp;

        [Header("Game Components")]
        public int handLimit = 7;
        public int turnCount = 0;
        public int numberOfSummons = 1;
        public bool viewingDeck;
        public bool viewingGrave;

        [Header("UI")]
        [SerializeField] private CombatManagerGUI combatUI;
        
        public Player player;
        public Player opponent;

        #endregion

        #region Initialisation

        private void Start()
        {
            StartBattle();
        }

        [ContextMenu("Start Battle")]
        private void StartBattle()
        {
            turnIndex = 0;
        }

        public void StartBattle(BattleData _data)
        {
            data = _data;

            boardManager.mainDeck.deckList = player.deck;
            boardManager.mainDeckOpp.deckList = opponent.deck;
            
            turnIndex = 0;
        }

        public void SetPlayer(Player _player)
        {
            player = _player;
        }

        public void SetOpponent(Player _opponent)
        {
            opponent = _opponent;
        }

        public bool IsPlayerTurn()
        {
            bool playerTurn = turnSteps[turnIndex].GetType() == typeof(StepPlayerTurn);

            return playerTurn;
        }

        #endregion

        #region Updating & Progression

        private IEnumerator UpdateState(int _index)
        {
            yield return turnSteps[_index].Execute();
        }
        
        public bool CheckEndgame()
        {
            if (player.life == 0 || opponent.life == 0)
            {
                GoTo<StepEndBattle>();
                return true;
            }

            return false;
        }

        public void GoTo<T>()
        {
            int newIndex = -1;
            for (int i = 0; i < turnSteps.Count; i++)
            {
                if (turnSteps[i].GetType() == typeof(T))
                {
                    newIndex = i;
                    break;
                }
            }

            if (newIndex > -1 && newIndex < turnSteps.Count)
            {
                turnIndex = newIndex;
                Debug.Log("Going to " + turnSteps[newIndex].GetType().ToString());
            }
            else
            {
                Debug.LogError("The TurnStep you're looking for doesn't exist");
                return;
            }
        }

        public void GoToNext()
        {
            if (turnIndex >= turnSteps.Count - 1)
            {
                Debug.Log("No more steps in TurnBasedController. Moving to end.");
                return;
            }

            Debug.Log($"TurnBasedController.GoToNext() from {turnSteps[turnIndex].gameObject.name} to {turnSteps[turnIndex + 1].gameObject.name}", this);

            turnIndex += 1;
        }

        public void GoToEnd()
        {
            turnIndex = turnSteps.Count - 1;
        }

        #endregion

        #region Player/Enemy Helpers

        public Player GetPlayer()
        {
            return player;
        }

        public Player GetOpponent()
        {
            return opponent;
        }

        #endregion

        #region Board Manager Accessors

        public void Draw(bool _isPlayer)
        {
            boardManager.DrawCard(_isPlayer);
        }
        
        public void PlaceCard(UICard _card, bool _isPlayer)
        {
            if (_card.cardInfo.GetType() == typeof(Monster))
            {
                boardManager.PlaceUnit(_card, _isPlayer);
            }
            else if (_card.cardInfo.GetType() == typeof(Sorcery))
            {
                boardManager.PlaceTactic(_card, _isPlayer);
            }
        }

        public void PlaceCard(UICard _card, UICardZone _zone)
        {
            GetInfoWindow(0).SetCard(_card);

            if (_card.cardInfo.GetType() == typeof(Monster))
            {
                boardManager.PlaceUnit(_card, _zone);
            }
            else if (_card.cardInfo.GetType() == typeof(Sorcery))
            {
                boardManager.PlaceTactic(_card, _zone);
            }
        }

        public UIHand GetHand(bool _isPlayer)
        {
            return _isPlayer ? boardManager.hand : boardManager.handOpp;
        }

        public void MoveCard(Card _card, string _instanceId, ZoneType _zoneType)
        {
            boardManager.MoveCard(_card, _instanceId, _zoneType);
        }

        public void MoveCard(string _instanceId, ZoneType _zoneType, bool _isPlayer)
        {
            boardManager.MoveCard(_instanceId, _zoneType, _isPlayer);
        }

        public void MoveCardToGrave(int _zoneIndex)
        {
            UICard card = combatUI.cardZones[_zoneIndex].CardInZone;
            Debug.LogFormat("Found {0} in zone {1}", card, _zoneIndex);
            boardManager.MoveCardToGrave(card);
        }

        #endregion

        #region Combat UI Accessors

        public void StartTurn()
        {
            combatUI.StartTurn();
        }

        public InfoWindow GetInfoWindow(int _windowIndex)
        {
            return combatUI.infoWindows[_windowIndex];
        }
        
        public void ShowInfoWindow(int _windowIndex, UICard _selectedCard)
        {
            combatUI.ShowInfoWindow(_windowIndex, _selectedCard);
        }

        public void SetPhaseButtonInteractable(bool _value)
        {
            combatUI.endTurnButton.GetComponent<UnityEngine.UI.Button>().interactable = _value;
        }

        public UICardZone GetZone(int _zoneIndex, bool _isPlayer)
        {
            return _isPlayer ? combatUI.cardZones[_zoneIndex] :
                               combatUI.cardZonesOpp[_zoneIndex];
        }

        public List<UICardZone> GetAllZones(bool _isPlayer)
        {
            return _isPlayer ? combatUI.cardZones : combatUI.cardZonesOpp;
        }

        public UICardZone FindCardInZones(string _instanceId, bool _isPlayer = true)
        {
            UICardZone selectedCard = null;

            if (_isPlayer)
            {
                selectedCard = combatUI.cardZones.Find((id) =>
                {
                    return id.InstanceId == _instanceId;
                });
            }
            else
            {
                selectedCard = combatUI.cardZonesOpp.Find((id) =>
                {
                    return id.InstanceId == _instanceId;
                });
            }

            return selectedCard;
        }

        #endregion

        #region Combat Manager Accessors
        
        public bool IsAttacking()
        {
            return combatManager.selectingAttackTarget;
        }

        public void SetAttackTarget(UICardZone _zone)
        {
            combatManager.attackTarget = _zone;
        }

        #endregion

        public void PassTurn()
        {
            // battleManager.ClearAvailableZones();

            if (turnSteps[turnIndex].GetType() == typeof(StepPlayerTurn))
            {
                GoTo<StepOpponentsTurn>();
            }
            else
            {
                GoTo<StepPlayerTurn>();
                StartTurn();
            }

            turnCount++;
        }
    }

    [System.Serializable]
    public class Player
    {
        public string playerName;
        public Deck deck;
        public int life;
    }
}