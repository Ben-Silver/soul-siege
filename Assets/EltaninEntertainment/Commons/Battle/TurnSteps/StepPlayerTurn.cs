using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.Commons.Battle
{
    public class StepPlayerTurn : TurnStep
    {
        public override IEnumerator Execute()
        {
            TurnBasedController.Instance.numberOfSummons = 1;
            yield return new WaitForSeconds(0.1f);

            TurnBasedController.Instance.Draw(true);
            yield return new WaitForSeconds(1.0f);

            TurnBasedController.Instance.StartTurn();

            yield break;
        }
    }
}