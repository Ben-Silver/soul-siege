using EltaninEntertainment.SoulSiege.Components;
using EltaninEntertainment.SoulSiege.Components.Interactables;
using EltaninEntertainment.SoulSiege.Enums.Cards;
using System.Collections;
using UnityEngine;

namespace EltaninEntertainment.Commons.Battle
{
    public class StepOpponentsTurn : TurnStep
    {
        public override IEnumerator Execute()
        {
            TurnBasedController.Instance.numberOfSummons = 1;
            yield return new WaitForSeconds(0.1f);

            TurnBasedController.Instance.Draw(false);
            yield return new WaitForSeconds(1.0f);

            yield return TakeTurn();
        }

        private IEnumerator TakeTurn()
        {
            yield return new WaitForSeconds(1.0f);

            UIHand hand = TurnBasedController.Instance
                                             .GetHand(false);

            int numberOfSummons = TurnBasedController.Instance
                                                     .numberOfSummons;

            int turnCount = TurnBasedController.Instance
                                               .turnCount;

            // Opponent summons monster and activates shit
            foreach (UICard card in hand.handSlots)
            {
                if (card.cardInfo.cardType == CardType.Unit && numberOfSummons > 0)
                {
                    TurnBasedController.Instance
                                       .PlaceCard(card, false);

                    yield return new WaitForSeconds(0.5f);
                    break;
                }
            }

            yield return new WaitForSeconds(1.0f);

            // If opponent controls monster that has higher
            // attack, it will try to enter battle phase
            if (turnCount > 0)
            {
                Battle();
            }
            else
            {
                Complete();
            }
        }

        private void Battle()
        {
            Complete();
        }

        protected override void Complete()
        {
            TurnBasedController.Instance.PassTurn();
        }
    }
}