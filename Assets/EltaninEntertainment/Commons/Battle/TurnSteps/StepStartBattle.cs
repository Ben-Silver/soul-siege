using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace EltaninEntertainment.Commons.Battle
{
    public class StepStartBattle : TurnStep
    {
        #region Variables

        public int startingHandSize = 5;

        #endregion

        #region Main Sequence

        public override IEnumerator Execute()
        { 
            yield return StartCoroutine(DrawStartingHand());

            bool playerTurn = Random.Range(0, 2) == 0;

            if (playerTurn)
                TurnBasedController.Instance
                                   .GoTo<StepPlayerTurn>();
            else
                TurnBasedController.Instance
                                   .GoTo<StepOpponentsTurn>();

            yield break;
        }

        #endregion

        #region Draw Opening Hand

        /// <summary>
        /// Function to give each player their starting hand
        /// </summary>
        public IEnumerator DrawStartingHand()
        {
            yield return new WaitForSeconds(0.1f);

            for (int i = 0; i < startingHandSize; i++)
            {
                TurnBasedController.Instance
                                   .Draw(true);

                yield return new WaitForSeconds(0.1f);
            }
            for (int i = 0; i < startingHandSize; i++)
            {
                TurnBasedController.Instance
                                   .Draw(false);

                yield return new WaitForSeconds(0.1f);
            }

            TurnBasedController.Instance
                               .SetPhaseButtonInteractable(true);
        }

        #endregion
    }
}