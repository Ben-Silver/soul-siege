using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.Commons.Battle
{
    public class StepEndBattle : TurnStep
    {
        #region Variables

        public enum BattleResult { Win, Lose, Tie }
        private BattleResult result;

        private bool resultSet = false;

        #endregion

        #region Main Sequence

        public override IEnumerator Execute()
        {
            SetResult();

            yield return EndTheBattle();
        }

        #endregion

        #region Battle End

        private IEnumerator EndTheBattle()
        {
            switch (result)
            {
                case BattleResult.Win:
                    {
                        Debug.Log("You defeated the enemies!");

                        yield return BattleWon();

                        break;
                    }
                case BattleResult.Lose:
                    {
                        Debug.Log("You were defeated!");

                        yield return BattleLost();

                        break;
                    }
                case BattleResult.Tie:
                    {
                        Debug.Log("Got away safely!");

                        yield return BattleTied();

                        break;
                    }
            }

            BattleManager.Instance.EndBattle();
        }

        #endregion

        #region Results

        private void SetResult()
        {
            if (!resultSet)
            {
                resultSet = true;

                bool outcome = true;

                result = outcome ? BattleResult.Win : BattleResult.Lose;
            }
        }

        public void SetResult(int _result)
        {
            if (!resultSet)
            {
                resultSet = true;

                result = (BattleResult)_result;
            }
        }

        private IEnumerator BattleWon()
        {
            yield return null;
        }

        private IEnumerator BattleLost()
        {
            yield return null;
        }

        private IEnumerator BattleTied()
        {
            yield return null;
        }

        #endregion
    }
}