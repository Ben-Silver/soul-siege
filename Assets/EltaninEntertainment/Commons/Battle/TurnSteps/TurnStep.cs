using System.Collections;
using UnityEngine;

namespace EltaninEntertainment.Commons.Battle
{
    public abstract class TurnStep : MonoBehaviour
    {
        public abstract IEnumerator Execute();

        protected virtual void Complete() 
        {
            TurnBasedController.Instance.GoToNext();
        }
    }
}