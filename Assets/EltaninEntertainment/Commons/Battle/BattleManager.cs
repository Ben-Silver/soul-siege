using EltaninEntertainment.Commons.Movement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace EltaninEntertainment.Commons.Battle
{
    public abstract class BattleManager : Singleton<BattleManager>
    {
        public SceneReference battleScene;

        private TempData tempData;

        public void StartBattle(BattleData _data)
        {
            tempData = new TempData
            (
                FindObjectOfType<PlayerController2D>().transform.position,
                FindObjectOfType<PlayerController2D>().direction,
                SceneManager.GetActiveScene().name
            );

            SceneManager.LoadSceneAsync(battleScene)
                        .completed += (AsyncOperation) =>
                        {
                            //Debug.Log("Loaded into the battle scene");
                            //GameObject.Find("BattleBack")
                            //          .GetComponent<SpriteRenderer>()
                            //          .sprite = _data.battleback;

                            //PauseMenu.Instance?.ToggleMenuButton(false);
                            TurnBasedController.Instance.StartBattle(_data);
                        };
        }

        public void EndBattle()
        {
            SceneManager.LoadSceneAsync(tempData.mapName)
                        .completed += (AsyncOperation) =>
                        {
                            FindObjectOfType<PlayerController2D>().transform.position = tempData.position;
                            FindObjectOfType<PlayerController2D>().direction = tempData.direction;

                            //PauseMenu.Instance?.ToggleMenuButton(true);

                            tempData = null;
                        };
        }
    }

    [System.Serializable]
    public class TempData
    {
        public Vector2 position
        {
            get;
            private set;
        }
        public Vector2 direction
        {
            get;
            private set;
        }
        public string mapName
        {
            get;
            private set;
        }

        public TempData(Vector2 _position, Vector2 _direction, string _mapName)
        {
            position = _position;
            direction = _direction;
            mapName = _mapName;
        }
    }
}