﻿using EltaninEntertainment.SoulSiege.Components.Interactables;
using EltaninEntertainment.SoulSiege.Components.Views;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace EltaninEntertainment.Commons.Battle
{
    /// <summary>
    /// Manages all the UI components for the GameManager
    /// </summary>
    public class CombatManagerGUI : MonoBehaviour
    {
        #region Variables

        [Header("Text Components")]
        public TextMeshProUGUI playerLifeText;
        public TextMeshProUGUI opponentLifeText;
        public TextMeshProUGUI turnCountText;
        public TextMeshProUGUI currentTurnText;

        [Header("Board Components")]
        public List<UICardZone> cardZones;
        public List<UICardZone> cardZonesOpp;
        public GameObject endTurnButton;
        
        [Header("Window Components")]
        public List<InfoWindow> infoWindows;
        public CardActionWindow cardActionWindow;
        public DeckListWindow deckListWindow;

        #endregion

        // Update is called once per frame
        void Update()
        {
            if (TurnBasedController.Instance.turnIndex < 0) return;

            bool playerTurn = TurnBasedController.Instance
                                                 .IsPlayerTurn();

            int turnCount = TurnBasedController.Instance
                                               .turnCount;

            Player player = TurnBasedController.Instance
                                               .GetPlayer();
            
            Player opponent = TurnBasedController.Instance
                                                 .GetOpponent();

            string playerName = player.playerName;
            string opponentName = opponent.playerName;

            currentTurnText.text = (playerTurn) ? playerName + "'s Turn" : opponentName + "'s Turn";

            playerLifeText.text = playerName + ": " + player.life;
            opponentLifeText.text = opponentName + ": " + opponent.life;

            turnCountText.text = "Turn " + turnCount;
        }
        
        public void ShowInfoWindow(int _windowIndex, UICard _selectedCard)
        {
            infoWindows[_windowIndex].gameObject.SetActive(true);

            infoWindows[_windowIndex].SetCard(_selectedCard);

            TurnBasedController.Instance.viewingDeck = true;
        }

        #region Starting and Ending Turn

        public void StartTurn()
        {
            endTurnButton.SetActive(true);
        }

        public void EndTurn()
        {
            TurnBasedController.Instance.PassTurn();
            endTurnButton.SetActive(false);
        }

        #endregion
    }
}