using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Singleton<T> : MonoBehaviour where T : Component
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (applicationIsQuitting || !instance)
                return null;

            return instance;
        }
    }
    private static bool applicationIsQuitting = false;

    [Header("Singleton")]
    public bool dontDestroyOnLoad = true;

    protected virtual void Awake()
    {
        if (!instance)
        {
            instance = this as T;

            if (dontDestroyOnLoad)
                DontDestroyOnLoad(gameObject);
        }
        else
        {
            Debug.LogFormat("{0}: {1} Destroying: {2}", name, instance.name, gameObject.name);
            Destroy(gameObject);
        }
    }

    #region The Old way
    /*
    #region Variables

    private static T instance;
    public static T Instance
    {
        get
        {
            if (applicationIsQuitting || !instance)
            {
                return null;
            }

            if (instance == null)
            {
                instance = FindObjectOfType<T>();

                if (instance == null)
                {
                    GameObject obj = new GameObject(typeof(T).Name);
                    instance = obj.AddComponent<T>();
                }
            }

            return instance;
        }
    }

    private static bool applicationIsQuitting = false;
    public bool dontDestroyOnLoad = true;

    #endregion

    #region Initialisation

    protected virtual void Awake()
    {
        if (instance == null)
        {
            instance = this as T;
        }
        else
        {
            Debug.LogFormat("{0}: {1} Destroying: {2}", name, instance.name, gameObject.name);
            Destroy(gameObject);
        }

        if (dontDestroyOnLoad)
            DontDestroyOnLoad(gameObject);
    }

    protected virtual void OnDestroy()
    {
        applicationIsQuitting = true;
    }

    #endregion
    */
    #endregion
}